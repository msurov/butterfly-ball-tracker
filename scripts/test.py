import cv2
import numpy as np
from pattern_detection import detect_quad_pattern
import matplotlib.pyplot as plt

A = np.reshape([1.27267872109548943627e+03, 0.00000000000000000000e+00, 6.41181292761807981151e+02, 0.00000000000000000000e+00, 1.27325243823878327021e+03, 5.30524906863849309957e+02, 0.00000000000000000000e+00, 0.00000000000000000000e+00, 1.00000000000000000000e+00], (3,3))
dist = np.array([-1.15504943769274975862e-01, 1.75073292885123438234e-01, 0.00000000000000000000e+00, 0.00000000000000000000e+00, -1.30067198780603465735e-03])


def apply_homography(H, pts):
    _pts = np.array([pts], dtype=float)
    return cv2.perspectiveTransform(_pts, H)[0]


def get_shape():
    return np.loadtxt('plates-shape-orig.txt', dtype=float) * 1e+3

def find_homography(real_points, img_points):
    H,_ = cv2.findHomography(np.array(real_points, dtype=float), np.array(img_points, dtype=float))
    return H


def reproject_points(A, dist, A_new, img_points):
    '''
          img_points <-- A <-- dist <-- P <-- real_world_points
               |
                ----> inv(A)' --> inv(dist)
                                      |
        new_img_points <--- A_new <---
    '''
    src = np.array([img_points], dtype='float32')
    dst = cv2.undistortPoints(src, A, dist, P=A_new)
    return dst[0,:,:]

im = cv2.imread(R'../dump/rects/Image__2017-03-04__20-29-56.bmp')
pts = detect_quad_pattern(im)
pts = np.array(pts, dtype=float)
pts_fixed = reproject_points(A, dist, A, pts)
real_coords = np.array([
    [-12.5, 41.5],
    [ 12.5, 41.5],
    [ 12.5, 61.5],
    [-12.5, 61.5],
], dtype=float)
H = find_homography(real_coords, pts_fixed)
shape = get_shape()


''' In image coordinates '''
shape_mapped = apply_homography(H, shape)
f = plt.figure(dpi=90)
ax = f.add_subplot(111)
ax.imshow(cv2.cvtColor(im, cv2.COLOR_BGR2RGB))
plt.plot(shape_mapped[:,0], shape_mapped[:,1])
plt.plot(pts_fixed[:,0], pts_fixed[:,1])
plt.axis('equal')
plt.grid()
plt.savefig('../dump/rects/detected.pdf')
plt.show()



''' In real coordinates
pts_mapped = apply_homography(np.linalg.inv(H), pts_fixed)
plt.plot(pts_mapped[:,0], pts_mapped[:,1])
plt.plot(shape[:,0], shape[:,1])
plt.axis('equal')
plt.grid()
plt.show()
'''
