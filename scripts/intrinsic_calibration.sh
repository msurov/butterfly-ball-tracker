#!/bin/bash

set -e

python intrinsic_calibration.py --srcpath=../dump/*.bmp --pattern=9x6 --saveto=intrinsics.json --outdir=../dump/out/ --iteration=3
