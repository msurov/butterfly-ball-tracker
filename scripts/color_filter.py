import numpy as np
import cv2
from os.path import join, splitext, basename
import argparse
from dataclasses import dataclass
import json


def point_line_distance(pts, line_coefs):
    v = line_coefs[0:3]
    p = line_coefs[3:6]
    d = pts - p
    e = d - np.outer(d.dot(v) / v.dot(v), v)
    return np.linalg.norm(e, axis=1)

def point_line_projection(pts, line_coefs):
    v = line_coefs[0:3]
    p = line_coefs[3:6]
    d = pts - p
    return d.dot(v) / v.dot(v)

def remove_overlighted(points, diap=(0.10, 0.99)):
    m = np.min(points, axis=1) < 0.5
    m &= np.max(points, axis=1) > 0.1
    return points[m]

@dataclass
class ColorFilter:
    v : np.ndarray
    p0 : np.ndarray
    t1 : float
    t2 : float
    r : float

    def asdict(self):
        a = np.zeros(6, float)
        a[0:3] = self.v
        a[3:6] = self.p0
        return {
            'axis': a.tolist(),
            'diap': [self.t1, self.t2],
            'radius': self.r
        }

def color_filter_distance(f : ColorFilter, im : np.ndarray):
    ny,nx,nc = im.shape
    assert nc == 3
    p = np.reshape(im, (-1,3))
    p = p / np.float32(255)
    t = (p - f.p0).dot(f.v)
    dt = np.maximum(t - f.t2, 0) + np.maximum(f.t1 - t, 0)
    dp = np.linalg.norm(np.outer(t, f.v) + f.p0 - p, axis=1)
    dp = np.maximum(dp - f.r, 0)
    d = dp + dt
    return np.reshape(d, (ny, nx))

@dataclass 
class Line:
    v : np.array
    p0 : np.array

def line_through_points(p1, p2) -> Line:
    v = p2 - p1
    nv = np.linalg.norm(v)
    if nv < 1e-8:
        return None
    v = v / nv
    p0 = (p1 + p2) / 2
    return Line(v = v, p0 = p0)

def line_points_distance(line : Line, pts):
    t = (pts - line.p0).dot(line.v)
    dp = np.linalg.norm(np.outer(t, line.v) + line.p0 - pts, axis=1)
    return dp

def fit_line_lstsq(pts):
    p0 = np.mean(pts, axis=0)
    dp = pts - p0
    evals, evecs = np.linalg.eig(dp.T @ dp)
    i = np.argmax(evals)
    v = evecs[:,i]
    return Line(v = v, p0 = p0)

def fit_line_ransac(pts, threshold, inliersmax=0.9, maxiter=100) -> Line:
    n,_ = np.shape(pts)
    best_mask = None
    best_inliers = 0

    for i in range(maxiter):
        i1,i2 = np.random.choice(n, size=2, replace=False)
        p1 = pts[i1]
        p2 = pts[i2]
        line = line_through_points(p1, p2)
        d = line_points_distance(line, pts)
        mask = d < threshold
        inliers = np.sum(mask)
        if inliers > best_inliers:
            best_inliers = inliers
            best_mask = mask
            if best_inliers > inliersmax * n:
                break

    return fit_line_lstsq(pts[best_mask]), best_mask

def fit_color_filter(pts : np.ndarray) -> ColorFilter:
    line, mask = fit_line_ransac(pts, 0.05)
    pts = pts[mask]
    v = line.v
    p0 = line.p0
    t = (pts - p0).dot(v)
    t1 = np.percentile(t, 5)
    t2 = np.percentile(t, 95)
    dt = t2 - t1
    t1 -= 0.05 * dt
    t2 += 0.05 * dt
    dp = np.linalg.norm(np.outer(t, v) + p0 - pts, axis=1)
    r = np.percentile(dp, 95) + 0.05
    r *= 1.05
    return ColorFilter(v = v, p0 = p0, t1=t1, t2=t2, r=r)

@dataclass
class Circle:
    x : float
    y : float
    radius : float

def detect_circle(ball_diameter, img) -> Circle:
    img2 = img.copy()
    img2 = cv2.medianBlur(img2, 5)
    scale = 1.0 * img.shape[0] / img2.shape[0]
    circles = cv2.HoughCircles(
        img2, cv2.HOUGH_GRADIENT, 2, 
        img2.shape[0], param1=200, param2=100,
        minRadius=int(ball_diameter * 0.5 * 0.9 / scale), 
        maxRadius=int(ball_diameter * 0.5 * 1.1 / scale)
    )
    if circles is None:
        return None
    circle = circles[0] * scale
    return Circle(x = circle[0,0], y = circle[0,1], radius = circle[0,2])

def draw_circle(im, circle : Circle, color : tuple, filled=False):
    x = np.round(circle.x).astype(np.int32)
    y = np.round(circle.y).astype(np.int32)
    r = np.round(circle.radius).astype(np.int32)
    cv2.circle(im, (x, y), r, color=color, thickness=-1 if filled else 1)

def get_pixels_inside_circle(im, circle : Circle):
    mask = np.zeros(im.shape[0:2], np.uint8)
    draw_circle(mask, circle, color=255, filled=True)
    mask = mask.astype(np.bool_)
    return im[mask]

def get_name(path):
    return splitext(basename(path))[0]

def json_save(js, path):
    with open(path, 'w') as f:
        json.dump(js, f, indent=2)

class Application:
    def __init__(self, diameter : float, dumpdir : str=None, saveto : str=None) -> None:
        self.dumpdir = dumpdir
        self.diameter = diameter
        self.saveto = saveto
    
    def __imdump(self, img : np.ndarray, name : str):
        if self.dumpdir is not None:
            cv2.imwrite(join(self.dumpdir, name) + '.png', img)

    def __detect_circle(self, img, name):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        circle = detect_circle(self.diameter, gray)
        return circle

    def process_sample(self, impath):
        name = get_name(impath)
        img = cv2.imread(impath)
        circle = self.__detect_circle(img, name)
        if circle is None:
            print(f'sample {name}: could not find image')
            return None

        print(f'sample {name}: circle detected at {circle.x}, {circle.y}')

        if self.dumpdir is not None:
            d = img.copy()
            draw_circle(d, circle, color=(128, 128, 0))
            self.__imdump(d, f'{name}-hough')

        pixels = get_pixels_inside_circle(img, circle)
        pixels = pixels / np.float32(255)
        return circle, pixels

    def __sample_apply_filter(self, filter, circle, impath):
        name = get_name(impath)
        img = cv2.imread(impath)
        dist = color_filter_distance(filter, img)
        self.__imdump((dist * 255).astype(np.uint8), f'{name}-filter-distance')

        if circle is not None:
            filtered = dist < 0.01
            m = np.zeros(img.shape[0:2], np.uint8)
            draw_circle(m, circle, 255, True)
            m = m.astype(np.bool_)

            inliers = filtered & m
            false_positive = filtered & ~m
            false_negative = ~filtered & m

            d = np.zeros(img.shape, np.uint8)
            d[inliers] = [0, 255, 0]
            d[false_positive] = [0, 0, 255]
            d[false_negative] = [255, 0, 0]
            self.__imdump(d, f'{name}-filtered')

            tot = np.sum(m)
            npos = np.sum(false_positive)
            nneg = np.sum(false_negative)

            print(f'sample {name}')
            print(f'  false positive: {npos * 100 / tot:2.2f}%')
            print(f'  false negative: {nneg * 100 / tot:2.2f}%')
    
    def __save_filter(self, filter : ColorFilter):
        if self.saveto is not None:
            js = filter.asdict()
            json_save(js, self.saveto)

    def process_samples(self, inputs):
        all_pixels = []
        all_cirles = []

        for impath in inputs:
            result = self.process_sample(impath)
            if result is None:
                all_cirles.append(None)
                continue
            
            circle, pixels = result
            all_cirles.append(circle)
            all_pixels.append(pixels)

        all_pixels = np.concatenate(all_pixels)
        if len(all_pixels) < 100:
            print('failed: not enough point for fitting color filter')
            return None

        filter = fit_color_filter(all_pixels)
        print('filter parameters:')
        print(f'  v:  {filter.v}')
        print(f'  p0: {filter.p0}')
        print(f'  t:  {filter.t1}, {filter.t2}')
        print(f'  r:  {filter.r}')

        for circle, impath in zip(all_cirles, inputs):
            self.__sample_apply_filter(filter, circle, impath)
        
        self.__save_filter(filter)
        

def main():
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--dumpdir', default=None, help='path to directrory to save intermediate data')
    parser.add_argument('--diameter', required=True, type=float, help='diameter of the ball in pixels')
    parser.add_argument('--saveto', default=None, type=str, help='the filter parameters will be written to this file')
    parser.add_argument('inputs', help='path to files to process', nargs='+')
    args = parser.parse_args()
    inputs = args.inputs

    app = Application(args.diameter, args.dumpdir, args.saveto)
    app.process_samples(inputs)

    # if circle is None:
    #     
    # else:
    #     self.dump_found_circle(img, circle)

main()
