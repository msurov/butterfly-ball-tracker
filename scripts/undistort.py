import cv2
import argparse
import numpy as np
import re
from os import listdir
from os.path import isfile, join, split, isdir, splitext
import json
import fnmatch


def parse_value(value):
    value = value.strip()
    ans = re.match(r'\[\s*(.*)\s*\]', value)
    if ans is not None:
        data, = ans.groups()
        elems = [e.strip() for e in data.split(',')]
        if len(elems) == 0:
            return []

        ans = re.match(r'[+-]?\d*\.?\d*[eE]?[+-]?\d*', elems[0])
        if ans is not None:
            return np.array([float(e) for e in elems])

        raise Exception('unsupported value fromat')

    ans = re.match(r'\"(.*)\"', value)
    if ans is not None:
        data, = ans.groups()
        return str(data)

    ans = re.match(r'[+-]?\d*\.?\d*[eE]?[+-]?\d*', value)
    if ans is not None:
        return float(value)

    raise Exception('unsupported value fromat')


def parse_config(configfile):
    result = {}

    with open(configfile) as f:
        for line in f:
            line = line.strip()
            if len(line) == 0:
                continue
            var,val = line.split('=')
            var = var.strip()
            val = val.strip()
            result[var] = parse_value(val)

    return result


def parse_mask(path):
    if isdir(path):
        return path, r'.*'

    if isfile(path):
        return path, ''

    return split(args.srcpath)


def read_calib_data(calibfile):
    _,ext = splitext(calibfile)
    if ext == '.txt':
        vals = parse_config(calibfile)
        A = vals['camera_matrix']
        distortion = vals['camera_distortion']
    elif ext == '.json':
        f = open(calibfile)
        vals = json.load(f)
        p = vals['camera_parameters']
        A = np.reshape(p['A'], newshape=(3,3))
        distortion = np.array(p['distortion'])
    else:
        raise Exception('unsupported calib file format')

    return A, distortion


def is_integer(v):
    return v - int(v) == 0


def get_mat_shape(size):
    if is_integer(np.sqrt(size / 12.)):
        a = int(np.sqrt(size / 12.))
        w = a * 4
        h = a * 3
    elif is_integer(np.sqrt(size / 20.)):
        a = int(np.sqrt(size / 20.))
        w = a * 5
        h = a * 4
    else:
        return None

    return h,w


def imread(path):
    _,ext = splitext(path)
    if ext == '.raw':
        data = np.fromfile(path, 'uint8')
        shape = get_mat_shape(len(data))
        data = np.reshape(data, newshape=shape)
        img = cv2.demosaicing(data, cv2.COLOR_BayerRG2BGR)
        return img
    else:
        return cv2.imread(path)


def undistort(camera_matrix, camera_distortion, srcfiles, dstfiles, crop=False):
    for (spath, dpath) in zip(srcfiles, dstfiles):
        _,name = split(spath)
        print 'undistorting %s..' % name,

        img = imread(spath)
        img_size = (img.shape[1], img.shape[0])
        # camera_matrix2, roi = cv2.getOptimalNewCameraMatrix(camera_matrix, camera_distortion, img_size, 1, img_size)
        camera_matrix2 = camera_matrix
        img2 = cv2.undistort(img, camera_matrix, camera_distortion, None, camera_matrix2)

        # if crop:
        #     x,y,w,h = roi
        #     img2 = img2[y:y+h, x:x+w]

        cv2.imwrite(dpath, img2)
        print 'ok'


def list_sources(srcdir, imgmask):
    files = listdir(srcdir)
    result = []

    for f in files:
        fpath = join(srcdir, f)
        if not isfile(fpath):
            continue
        if not fnmatch.fnmatch(f, imgmask):
            continue
        result.append(fpath)

    return result


def gen_result_name(srcpath):
    path,name = split(srcpath)
    basename,ext = splitext(name)
    if ext == '.raw':
        result = join(path, 'undistort-' + basename + '.png')
    else:
        result = join(path, 'undistort-' + name)
    return result


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--srcpath', required=True, help='path to directory containing photos')
    parser.add_argument('--outdir', help='path to directory to save undistorted photos')
    parser.add_argument('--config', help='path to the text file containing calibration data')
    parser.add_argument('--crop', type=bool, default=True, help='crop the result image or not')

    args = parser.parse_args()
    srcdir,imgmask = parse_mask(args.srcpath)
    outdir = args.outdir if args.outdir is not None else srcdir
    config = args.config
    crop = args.crop

    if len(imgmask) == 0:
        srcfiles = [srcdir]
    else:
        srcfiles = list_sources(srcdir, imgmask)

    camera_matrix, camera_distortion = read_calib_data(config)
    dstfiles = [gen_result_name(f) for f in srcfiles]
    undistort(camera_matrix, camera_distortion, srcfiles, dstfiles, crop)
