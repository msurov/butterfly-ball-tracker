import cv2
import numpy as np
import matplotlib.pyplot as plt

DEBUG = True
main_quad_sz = (70,100)


def normalized(v):
    return v / np.linalg.norm(v)


def clamp(v, vmin, vmax):
    return min(max(v, vmin), vmax)


def rotmat(angle):
    return np.array([
        [np.cos(angle), -np.sin(angle)],
        [np.sin(angle),  np.cos(angle)],
    ])


def cross(v1,v2):
    return v1[0] * v2[1] - v1[1] * v2[0]


class LineSeg:
    def __init__(self, p1=None, p2=None, v=None, a=None, b=None):
        if p1 is not None and v is not None:
            self.p = np.array(p1, dtype=float)
            self.v = np.array(v, dtype=float)
        elif p1 is not None and p2 is not None:
            self.p = np.array(p1, dtype=float)
            self.v = np.array(p2, dtype=float) - self.p
        elif a is not None and b is not None:
            self.p = np.array([0, b], dtype=float)
            self.v = np.array([1, a], dtype=float)
        else:
            raise Exception('not enough arguments')

    def intsct(self, line2):
        S = np.array([[0, -1], [1, 0]], dtype=float)
        t = line2.v.dot(S).dot(line2.p - self.p) / line2.v.dot(S).dot(self.v)
        return t

    def intsct_pt(self, line2):
        S = np.array([[0, -1], [1, 0]], dtype=float)
        t = line2.v.dot(S).dot(line2.p - self.p) / line2.v.dot(S).dot(self.v)
        return self.at(t)

    def at(self, t):
        return self.p + self.v * t

    def move(self, w):
        return LineSeg(p1=self.p + w, v=self.v)

    def move_orth(self, d):
        S = np.array([[0, -1], [1, 0]], dtype=float)
        w = S.dot(normalized(self.v)) * d
        return self.move(w)

    def __repr__(self):
        return '([%f,%f], [%f,%f])' % (self.p[0], self.p[1], self.v[0], self.v[1])

    def plot(self):
        pts = np.array([self.at(0), self.at(1)])
        plt.plot(pts[:,0], pts[:,1])

    def closest(self, pt):
        return (pt - self.p).dot(self.v) / self.v.dot(self.v)

    def dist(self, _pt):
        pt = np.array(_pt, dtype='float')
        t = self.closest(pt)
        t = clamp(t, 0, 1)
        return np.linalg.norm(pt - self.at(t))

    def shorter(self, d):
        p1 = self.at(0)
        p2 = self.at(1)
        c = (p1 + p2) / 2
        q1 = p1 + normalized(c - p1) * d / 2
        q2 = p2 + normalized(c - p2) * d / 2
        return LineSeg(p1=q1, p2=q2)

    def ang(self):
        return np.arctan2(self.v[1], self.v[0])

    def rotate(self,angle):
        R = rotmat(angle)
        return LineSeg(p1=R.dot(self.p), v=R.dot(self.v))


def get_lineseg_neighborhood(w, _pt1, _pt2, d=2):
    pt1 = np.array(_pt1, dtype='float')
    pt2 = np.array(_pt2, dtype='float')
    l = LineSeg(p1=pt1, p2=pt2)
    l = l.shorter(2 * d)

    wy,wx = np.shape(w)

    x1 = min(pt1[0], pt2[0])
    x2 = max(pt1[0], pt2[0])
    y1 = min(pt1[1], pt2[1])
    y2 = max(pt1[1], pt2[1])

    x1 = clamp(int(np.rint(x1 - d)), 0, wx - 1)
    x2 = clamp(int(np.rint(x2 + d)), 0, wx - 1)
    y1 = clamp(int(np.rint(y1 - d)), 0, wy - 1)
    y2 = clamp(int(np.rint(y2 + d)), 0, wy - 1)

    pts = []

    for y in xrange(y1, y2 + 1):
        for x in xrange(x1, x2 + 1):
            if l.dist([x,y]) < d:
                pts += [(x,y,w[y,x])]

    return pts


def fit_line(x,y,w):
    n = len(x)

    A = np.zeros((n,2), dtype=float)
    B = np.zeros((n,1), dtype=float)
    i = 0

    for i in xrange(0,n):
        if w[i] > 0:
            A[i,0] = x[i] * w[i]
            A[i,1] = 1. * w[i]
            B[i,0] = y[i] * w[i]
            i += 1

    A = A[0:i,:]
    B = B[0:i,:]

    _,ans = cv2.solve(A, B, flags=cv2.DECOMP_SVD)
    return ans[0], ans[1]


def refine_line_lsq(line0, _pts):
    pts = np.array(_pts, dtype=float)
    alpha = line0.ang()
    R = rotmat(-alpha)

    x,y = np.array([R.dot([x,y]) for x,y,_ in pts]).T
    w = pts[:,2]
    a,b = fit_line(x,y,w)
    l = LineSeg(a=a,b=b)
    l = l.rotate(alpha)
    return l


def inside_box(pt, box):
    x,y = pt
    x1,y1,x2,y2 = box
    return x >= x1 and x <= x2 and y >= y1 and y <= y2


def get_covering_box(_pts):
    pts = np.array(_pts, dtype=float)

    x = pts[:,0]
    y = pts[:,1]
    x1 = np.min(x)
    x2 = np.max(x)
    y1 = np.min(y)
    y2 = np.max(y)
    return x1,y1,x2,y2


def line_box_intersections(line,box):
    x1,y1,x2,y2 = box

    l1 = LineSeg(p1=(x1,y1), p2=(x1,y2))
    l2 = LineSeg(p1=(x1,y2), p2=(x2,y2))
    l3 = LineSeg(p1=(x2,y2), p2=(x2,y1))
    l4 = LineSeg(p1=(x2,y1), p2=(x1,y1))

    p1 = l1.intsct_pt(line)
    p2 = l2.intsct_pt(line)
    p3 = l3.intsct_pt(line)
    p4 = l4.intsct_pt(line)

    res = []

    if inside_box(p1, box):
        res += [p1]
    if inside_box(p2, box):
        res += [p2]
    if inside_box(p3, box):
        res += [p3]
    if inside_box(p4, box):
        res += [p4]

    return res


def refine_line_seg(weights_img, p1, p2):
    '''
        refine line crossing 2 points: p1 and p2
    '''
    l1 = LineSeg(p1=p1, p2=p2)
    pts = get_lineseg_neighborhood(weights_img, p1, p2, 3.)
    l2 = refine_line_lsq(l1, pts)
    return l2


def get_covering_rect(_pts):
    pts = np.array(_pts, dtype=float)
    x = pts[:,0]
    y = pts[:,1]
    return np.min(x), np.min(y), np.max(x), np.max(y)


def plot_line_seg(line, box, *args, **kwargs):
    p1,p2 = line_box_intersections(line, box)
    plt.plot([p1[0],p2[0]], [p1[1],p2[1]], *args, **kwargs)


def Edges(im):
    edges = np.sqrt((cv2.Sobel(im, cv2.CV_32F, 1, 0)**2 + cv2.Sobel(im, cv2.CV_32F, 0, 1)**2)) / 16.
    return edges

def refine_quad(im, quad):
    im2 = cv2.GaussianBlur(im, (3,3), -2.)
    edges = Edges(im)
    if DEBUG:
        cv2.imwrite(R'../dump/rects/edges.png', edges)

    l1 = refine_line_seg(edges, quad[0], quad[1])
    l2 = refine_line_seg(edges, quad[1], quad[2])
    l3 = refine_line_seg(edges, quad[2], quad[3])
    l4 = refine_line_seg(edges, quad[3], quad[0])

    pt1 = l1.intsct_pt(l2)
    pt2 = l2.intsct_pt(l3)
    pt3 = l3.intsct_pt(l4)
    pt4 = l4.intsct_pt(l1)

    refined = np.array([pt1, pt2, pt3, pt4])
    return refined


def find_main_quad(quads):
    d = np.linalg.norm(main_quad_sz)
    filtered = []

    for q in quads:
        d1 = np.linalg.norm(q[2] - q[0])
        d2 = np.linalg.norm(q[3] - q[1])

        if abs((d1 - d) / d) < 0.1 and abs((d2 - d) / d) < 0.1:
            filtered += [q]

    if len(filtered) != 1:
        return None

    return filtered[0]


def plot_poly(ax, pts, **kwargs):
    n = len(pts)
    for i in xrange(0, n):
        p1 = pts[i]
        p2 = pts[(i + 1) % n]
        ax.plot([p1[0], p2[0]], [p1[1], p2[1]], **kwargs)


def filter_quads(contours):
    filtered = []

    for c in contours:
        approximated = cv2.approxPolyDP(c, 4, True)
        if len(approximated) != 4:
            continue
        else:
            filtered += [np.reshape(approximated, (4, 2))]

    return filtered


def is_cont_inside_rect(rect, pts):
    x1,y1,x2,y2 = rect

    for x,y in pts:
        if x < x1 or x > x2 or y < y1 or y > y2:
            return False

    return True


def cmp_quads(q1, q2):
    return (np.linalg.norm(q2 - q1[0], axis=1).min() + \
        np.linalg.norm(q2 - q1[1], axis=1).min() + \
        np.linalg.norm(q2 - q1[2], axis=1).min() + \
        np.linalg.norm(q2 - q1[3], axis=1).min()) / 4.


def find_inside_cont(main_quad, quads):
    rect = get_covering_rect(main_quad)
    filtered = filter(lambda q: is_cont_inside_rect(rect, q), quads)
    filtered = filter(lambda q: cmp_quads(q, main_quad) > 5., filtered)

    print 'found small quads ', filtered
    if len(filtered) != 1:
        return None

    return filtered[0]


def sort_quad_verts(main_quad, quads):
    '''
       3               2
         ------------- 
        |             |
        |  -          |
        | | |         |
        |  -          |
         ------------- 
       0               1
    '''

    if len(main_quad) != 4:
        return None

    small_quad = find_inside_cont(main_quad, quads)
    if small_quad is None:
        print '[error]  Can\'t find small quad'
        return None

    c = np.mean(small_quad, axis=0)
    vecs = [q - c for q in main_quad]
    closest = np.argmin(np.linalg.norm(vecs, axis=1))
    o = main_quad[closest,:]
    p1 = main_quad[(closest+1) % 4,:]
    p2 = main_quad[(closest+2) % 4,:]
    p3 = main_quad[(closest+3) % 4,:]

    if cross(p1 - o, p3 - o) > 0:
        return [o,p3,p2,p1]
    return [o,p1,p2,p3]


def find_homography(world_pts, img_pts):
    return cv2.findHomography(world_pts, img_pts)


def get_plane_mat(A, dist, H):
    P_normalized = np.linalg.inv(A).dot(H)
    P_real = P_normalized /  np.linalg.norm(P_normalized[:,1])
    return P_real


def detect_quad_pattern(im):
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)

    # thresh
    thresholded = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 105, -10)
    if DEBUG:
        cv2.imwrite(R'../dump/rects/thresholded.png', thresholded)

    # contours
    _,contours,_ = cv2.findContours(thresholded, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    quads = filter_quads(contours)
    if DEBUG:
        im2 = im.copy()
        cv2.drawContours(im2, quads, -1, (255,0,0))
        cv2.imwrite(R'../dump/rects/contours.png', im2)

    main_quad = find_main_quad(quads)
    if main_quad is None:
        return None

    main_quad = refine_quad(gray, main_quad)
    o,px,pn,py = sort_quad_verts(main_quad, quads)

    if DEBUG:
        im2 = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        f = plt.figure(dpi=600)
        ax = f.add_subplot(111)
        ax.imshow(im2)
        plot_poly(ax, main_quad, c='r', lw=0.1)
        ax.annotate('o', xy=(o[0], o[1]), color='b')
        ax.annotate('x', xy=(px[0], px[1]), color='b')
        ax.annotate('y', xy=(py[0], py[1]), color='b')
        f.savefig(R'../dump/rects/detected.pdf')

    return o,px,pn,py
