import cv2
import numpy as np
import matplotlib.pyplot as plt
import json
from matplotlib.widgets import Button
from matplotlib.widgets  import RectangleSelector
from dataclasses import dataclass
import os
import re


def array2str(arr):
    elems = ['%.20e' % e for e in np.reshape(arr, np.size(arr))]
    return '[' + ', '.join(elems) + ']'

def reproject_points(A, dist, A_new, img_points):
    '''
          img_points <-- A <-- dist <-- P <-- real_world_points
               |
                ----> inv(A)' --> inv(dist)
                                      |
        new_img_points <--- A_new <---
    '''
    src = np.array([img_points], dtype='float32')
    dst = cv2.undistortPoints(src, A, dist, P=A_new)
    return dst[0,:,:]

def clarify_points(img, _points):
    points = np.array(_points, dtype='float32')
    points = np.reshape(points, (1, -1, 2))
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 1e-3)
    ans = cv2.cornerSubPix(gray, points, (7, 7), (-1,-1), criteria)
    if ans is not None:
        points = ans
    shape = np.shape(_points)
    points = np.reshape(points, shape)
    return points

@dataclass
class PlatesShape:
    plates_distance : float
    key_points : np.ndarray
    edgeline : np.ndarray

def load_shape(filepath) -> PlatesShape:
    with open(filepath) as data_file:
        data = json.load(data_file)
    key_points = data['key_points']
    key_points = np.reshape(key_points, (-1,2))
    shape = data['shape']
    shape = np.reshape(shape, (-1,2))
    plates_distance = data['plates_distance']
    return PlatesShape(plates_distance, key_points, shape)

@dataclass
class CameraIntrinsics:
    A : np.ndarray
    distortion : np.ndarray

def show_sample(rv, tv, impath, angle, shape : PlatesShape, intr : CameraIntrinsics):
    im = cv2.imread(impath)
    im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
    plt.imshow(im)
    h,w,_ = im.shape
    plt.plot([0, w, w, 0, 0], [0, 0, h, h, 0], color='b')
    curve = rotate_points(shape.edgeline, angle)
    world_points = np.array([[x,y,0] for (x,y) in curve], dtype='float32')
    img_points,_ = cv2.projectPoints(world_points, rv, tv, intr.A, intr.distortion)
    img_points = np.reshape(img_points, [-1,2])
    plt.plot(img_points[:,0],img_points[:,1])

def show_samples(rv, tv, samples, shape : PlatesShape, intr : CameraIntrinsics):
    for sample in samples:
        path,angle = sample
        fig = plt.figure(path)
        show_sample(rv, tv, path, angle, shape, intr)
    plt.show()

def load_intrinsics(config_path) -> CameraIntrinsics:
    with open(config_path) as data_file:
        data = json.load(data_file)
    campars = data['intrinsics']
    A = np.array(campars['K'], dtype='float32')
    A = A.reshape((3,3))
    distortion = np.array(campars['distortion'], dtype='float32')
    return CameraIntrinsics(A = A, distortion=distortion)

def pt_in_rect(pt : tuple, rect : tuple):
    px,py = pt
    rx,ry,rw,rh = rect
    return px >= rx and py >= ry and px < rx + rw and py < ry + rh

class PointsPicker:
    def __init__(self, im, npoints):
        self.fig, self.ax = plt.subplots(1,1)
        self.picked_points = []
        self.markers = []
        self.npoints = npoints
        self.im = im
        h,w,_ = im.shape
        self.imrect = (0, 0, w, h)
        self.ax.imshow(im, interpolation='nearest')
        plt.plot([0, w, w, 0, 0], [0, 0, h, h, 0], lw=0.1, color='black')

    def draw_cross(self, pt, color):
        x,y = pt
        l1, = self.ax.plot([x-5, x+5], [y,y], color=color)
        l2, = self.ax.plot([x, x], [y-5,y+5], color=color)
        return l1, l2

    def remove_cross(self, c):
        l1, l2 = c
        l1.remove()
        l2.remove()

    def remove_last_point(self):
        if len(self.picked_points) > 0:
            self.picked_points = self.picked_points[:-1]
            self.remove_cross(self.markers[-1])
            self.markers = self.markers[:-1]

    def onrelease(self, event):
        if event.key == 'backspace':
            self.remove_last_point()
            self.fig.canvas.draw()

    def add_point(self, pt):
        if pt_in_rect(pt, self.imrect):
            refined, = clarify_points(self.im, [pt])
            c = self.draw_cross(refined, 'b')
            self.markers.append(c)
            self.picked_points.append(refined)
            self.fig.canvas.draw()

    def onpress(self, event):
        if event.button == 1:
            pt = event.xdata, event.ydata
            self.add_point(pt)

        if len(self.picked_points) == self.npoints:
            plt.close()

    def run(self):
        press_connect = self.fig.canvas.mpl_connect('button_press_event', lambda event: self.onpress(event))
        release_connect = self.fig.canvas.mpl_connect('key_release_event', lambda event: self.onrelease(event))
        plt.show()

def pick_corners(img, npoints):
    _img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    picker = PointsPicker(_img, npoints)
    picker.run()
    if len(picker.picked_points) != npoints:
        return None
    return picker.picked_points

def find_all_corners(img_set):
    all_corners = []
    for img in img_set:
        corners = pick_corners(img)
        all_corners += [corners]

    return all_corners

def find_samples(folderpath):
    samples = []
    files = os.listdir(folderpath)
    for f in files:
        name,ext = os.path.splitext(f)
        if ext not in ['.bmp', '.png']:
            continue

        ans = re.match(r'theta=([\+\-\d\.Ee]+)', name)
        if ans is None:
            continue

        angle, = ans.groups()
        angle = float(angle)
        path = os.path.join(folderpath, f)
        sample = (path, angle)
        samples += [sample]

    return samples

def rotmat_2d(a):
    return np.array([
        [np.cos(a), -np.sin(a)],
        [np.sin(a),  np.cos(a)]
    ])

def rotate_points(points, angle):
    R = rotmat_2d(angle)
    new_points = [R.dot(p) for p in points]
    return np.array(new_points)

def process_sample(sample, shape : PlatesShape):
    path,angle = sample
    im = cv2.imread(path)
    img_points = pick_corners(im, len(shape.key_points))
    if img_points is None:
        return  None
    world_pts = rotate_points(shape.key_points, angle)
    world_pts = np.array([(x,y,0) for x,y in world_pts], dtype='float32')
    return world_pts, img_points

def process_samples(samples, shape : PlatesShape):
    all_world_points = np.zeros((0,3), dtype='float32')
    all_img_points = np.zeros((0,2), dtype='float32')

    for sample in samples:
        ans = process_sample(sample, shape)
        if ans is None:
            continue
        world_pts, img_points = ans
        all_world_points = np.vstack((all_world_points, world_pts))
        all_img_points = np.vstack((all_img_points, img_points))

    return all_world_points, all_img_points

def make_homogeneous_transform(rv, tv): 
    R,_ = cv2.Rodrigues(np.reshape(rv, [-1]))
    T = np.zeros((4,4), dtype='float32')
    T[0:3,0:3] = R
    T[0:3,3] = np.reshape(tv, (3,))
    T[3,3] = 1.
    return T

def decompose_homogeneous_transform(T):
    R = T[0:3,0:3]
    rv,_ = cv2.Rodrigues(R)
    tv = T[0:3,3]
    return rv,tv

def translation_matrix(d):
    T = np.eye(4, 4, dtype='float32')
    T[0:3,3] = np.reshape(d, (3,))
    return T

def load_extrinsics(jsonpath):
    with open(jsonpath, 'r') as f:
        data = json.load(f)
        rv = np.array(data['camera_parameters']['rv'], float)
        tv = np.array(data['camera_parameters']['tv'], float)
        return rv, tv

def save_extrinsics(jsonpath, rv, tv):
    d = {
        'camera_parameters': {
            'rv': rv.tolist(),
            'tv': tv.tolist()
        }
    }
    with open(jsonpath, 'w') as f:
        json.dump(d, f, indent=2)

def verify_extrinsics():
    intr = load_intrinsics('configs/intrinsics.json')
    shape = load_shape('configs/shape.json')
    rv, tv = load_extrinsics('configs/extrinsics.json')
    samples = find_samples('dump/calib-extrinsics')
    show_samples(rv, tv, samples, shape, intr)

def find_extrinsics():
    intr = load_intrinsics('configs/intrinsics.json')
    shape = load_shape('configs/shape.json')
    samples = find_samples('dump/calib-extrinsics')
    world_points, img_points = process_samples(samples, shape)
    _,rv,tv = cv2.solvePnP(world_points, img_points, intr.A, intr.distortion, flags=cv2.SOLVEPNP_ITERATIVE)
    save_extrinsics('configs/extrinsics.json', rv, tv)
    print('rv', np.reshape(rv, [-1]))
    print('tv', np.reshape(tv, [-1]))
    T = make_homogeneous_transform(rv, tv)
    print(T)
    print(decompose_homogeneous_transform(T))
    D = translation_matrix([0,0,-shape.plates_distance/2])
    T2 = T.dot(D)
    rv2,tv2 = decompose_homogeneous_transform(T2)
    print(T2)
    return rv, tv

if __name__ == '__main__':
    find_extrinsics()
    verify_extrinsics()
