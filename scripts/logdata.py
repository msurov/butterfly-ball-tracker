import numpy as np
import matplotlib.pyplot as plt;
import sys
sys.path.append('../../python-common/src/')
from common import scanf_textfile


data = scanf_textfile('[info] at %fs: %f: [%f, %f]', '../dump/log.txt')
data = np.array(data)

# plt.plot(data[:,2], data[:,3], '.')
# plt.grid()
# plt.show()

f, axarr = plt.subplots(nrows=1, ncols=2, sharey=True)
axarr[0].hist(data[:,2], bins=10)
axarr[0].grid()
axarr[1].hist(data[:,3], bins=10)
axarr[1].grid()
plt.show()
