#include "camera.h"

using namespace std;
using namespace cv;

typedef function<ICameraPtr(Json::Value const&)> CameraCreator;
static map<string,CameraCreator> creators;


struct Register
{
    Register(string const& name, CameraCreator const& creator)
    {
        creators.emplace(name, creator);
    }
};

#ifdef USE_CAMERA_BASLER
ICameraPtr create_basler_camera(Json::Value const& cfg);
static Register register_basler("basler", create_basler_camera);
#endif

#ifdef USE_CAMERA_FAKE
ICameraPtr create_fake_camera(Json::Value const& cfg);
static Register register_fake("basler", create_fake_camera);
#endif


ICameraPtr create_camera(Json::Value const& cfg)
{
    auto const& cam = json_get(cfg, "camera");
    auto const& name = json_get<string>(cam, "name");
    auto p = creators.find(name);
    if (p == creators.end())
        throw_runtime_error("unknown camera: ", name);
    ICameraPtr result = p->second(cam);
    return result;
}
