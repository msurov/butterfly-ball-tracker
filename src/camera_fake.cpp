#include <sys/types.h>
#include <vector>
#include <algorithm>
#include <thread>
#include <memory>

#include <cppmisc/traces.h>
#include <cppmisc/files.h>
#include <cppmisc/timing.h>
#include <cppmisc/strings.h>

#include "camera.h"
#include "cv_helpers.h"

using namespace std;
using namespace cv;


class FakeCamera;
typedef std::shared_ptr<FakeCamera> FakeCameraPtr;


class FakeCamera : public ICamera
{
private:
    CameraCallback          _handler;
    vector<string>          _files;
    unique_ptr<thread>      _caller;
    string                  _sources_format;
    bool                    _bexit;
    int64_t                 _period;


    int64_t get_timestamp(string const& filepath)
    {
        string&& name = getname(filepath);
        auto nameext = splitext(name);
        string&& digits = get_digits_substr(get<0>(nameext));
        if (digits.empty())
            return epoch_usec();
        return stoll(digits);
    }

    cv::Mat read(std::string const& path)
    {
        auto ext = getext(path);
        ext = tolower(ext);

        if (_sources_format == "gray")
        {
            if (ext == ".bmp" || ext == ".png")
                return cv::imread(path, IMREAD_GRAYSCALE);
        }
        else if (_sources_format == "bggr")
        {
            return read_raw(path);
        }

        // TODO: process other formats
        throw std::runtime_error("not implemented");
        return cv::Mat();
    }

    void handler_loop()
    {
        int64_t t0 = epoch_usec();
        int64_t t1 = t0;
        int i = 0;

        LoopRate rate(_period);

        while (!_bexit)
        {
            if (_handler)
            {
                Mat img = read(_files[i]);
                auto ts = get_timestamp(_files[i]);
                _handler((uint8_t const*)img.data, img.cols, img.rows, ts);
            }

            rate.wait();
            i = (i + 1) % _files.size();
        }
    }

public:
    FakeCamera(string const& files_mask, string const& sources_format)
    {
        if (sources_format != "bggr" && sources_format != "rggb" && sources_format != "gray")
            throw runtime_error("incorrect sources_format value");

        _bexit = true;
        _sources_format = sources_format;
        _files = move(get_files(files_mask));

        if (_files.empty())
            throw runtime_error("there are no files in " + files_mask);

        sort(_files.begin(), _files.end());
    }

    ~FakeCamera()
    {
        stop();
    }

    void set_handler(CameraCallback const& handler)
    {
        _handler = handler;
    }

    void run()
    {
        _bexit = false;
        _caller.reset(new thread([this]() { this->handler_loop(); }));
    }

    void stop()
    {
        _bexit = true;
        if (_caller && _caller->joinable())
            _caller->join();
    }

    string description()
    {
        return "Fake Camera";
    }

    string image_type()
    {
        return _sources_format;
    }
};


ICameraPtr create_fake_camera(Json::Value const& cfg)
{
    auto const& sources = json_get<string>(cfg, "sources");
    auto const& format = json_get<string>(cfg, "format");
    FakeCameraPtr cam = make_shared<FakeCamera>(sources, format);
    return cam;
}
