#include <opencv2/opencv.hpp>

#include <list>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <fstream>

#include <cppmisc/serializer.h>
#include <cppmisc/traces.h>
#include <cppmisc/argparse.h>
#include <cppmisc/signals.h>
#include <cppmisc/timing.h>
#include <networking/tcp.h>

#include "detector.h"
#include "camera.h"
#include "mathutils.h"
#include "imgdump.h"

using namespace std;
using namespace cv;


static unique_ptr<TCPSrv>       server;
static shared_ptr<Connection>   conct;
static unique_ptr<FindBall>     find_ball;
static bool                     terminate_app = false;


inline void kill_connection()
{
    conct = nullptr;
}

inline bool client_alive()
{
    return conct != nullptr;
}

static bool send_message(bool good, int64_t ts_usec, double x, double y)
{
    char buf[256];
    int len = ser::pack(buf, sizeof(buf), 
        "ts", ts_usec, 
        "good", good,
        "x", x,
        "y", y
    );
    int status = conct->write(buf, len);
    if (status <= 0)
    {
        dbg_msg("send failed: ", status);
        conct = nullptr;
        return false;
    }
    return true;
}

static void camera_handler(uint8_t const* data, int Nx, int Ny, int64_t frame_timestamp)
{
    if (!client_alive())
        return;

    static bool was_detected = true;

    try
    {
        Vec2d circle;
        bool bdetected;
        bool bsent;
        int64_t t;

        Mat raw(Ny, Nx, CV_8U, (void*)data);

        t = epoch_usec();
        bdetected = find_ball->process_raw(raw, circle);
        t = epoch_usec() - t;

        bsent = send_message(bdetected, frame_timestamp, circle[0], circle[1]);
        if (!bsent)
        {
            err_msg("can't send data; connection closed");
            kill_connection();
        }

        if (bdetected)
        {
            dbg_msg("detected ", circle, "; in ", t, "us");
        }
        else if (was_detected)
        {
            find_ball->dump(to_string(frame_timestamp) + "_");
            imgdump(to_string(frame_timestamp) + ".raw", raw, "bggr");
            warn_msg("ball lost");
        }

        was_detected = bdetected;
    }
    catch (exception& e)
    {
        kill_connection();
        err_msg(e.what());
    }
    catch (...)
    {
        kill_connection();
        err_msg("caught unknown exception");
    }
}

int run_tracker(Json::Value const& cfg)
{
    // Ctrl+C handler
    auto f = []() { 
        kill_connection(); 
        server->stop();
        terminate_app = true; 
    };
    SysSignals::instance().set_sigint_handler(f);
    SysSignals::instance().set_sigterm_handler(f);

    try
    {
        // init
        find_ball.reset(new FindBall);
        find_ball->init(cfg);

        auto const& jssrv = json_get(cfg, "server");
        int port = json_get<int>(jssrv, "port");
        if (!in_diap(port, 0, 65535))
            throw_invalid_argument("Expect port value ", port, " between 0 and 65535");
        server.reset(new TCPSrv(port));

        auto cam = create_camera(cfg);
        cam->set_handler(camera_handler);

        // server main loop
        while (!terminate_app)
        {
            info_msg("waiting for an incoming connection on port ", server->port());
            conct = server->wait_for_connection();
            info_msg("connection established");

            auto reader = ser::make_pack_reader([](char* p, int n) {
            	if (!conct)
            		return -1;
                return conct->read(p, n, true);
            });

            ser::Packet pack;
            int status = reader->fetch_next(pack);
            if (status < 0)
            {
                err_msg("incorrect request from clinet; connection broken;");
                conct = nullptr;
                continue;
            }

            if (pack.get<std::string>("cmd") == "start")
            {
                info_msg("got the 'start' command; measuring...");
                cam->run();
                while (client_alive())
                    sleep_usec(1e+6);
                cam->stop();
                info_msg("connection closed");
            }
            else
            {
                err_msg("got an unknown command: '", pack.get<std::string>("cmd"));
                kill_connection();
                continue;
            }
        }
    }
    catch (std::exception const& e)
    {
        err_msg(e.what());
        return -1;
    }
    catch (...)
    {
        err_msg("unknown error");
        return -1;
    }

    return 0;
}

int main(int argc, char const* argv[])
{
    Arguments args({
        Argument("-c", "config", "path to the butterfly robot config file", "", ArgumentsCount::One)
    });

    try
    {
        auto p = args.parse(argc, argv);
        string configpath = p["config"];
        auto cfg = json_load(configpath);
        if (json_has(cfg, "logger"))
            traces::init(json_get(cfg, "logger"));
        auto imgdump = imgdump_init(cfg);
        run_tracker(cfg);
    }
    catch (invalid_argument& e)
    {
        err_msg(e.what());
        cout << args.help_message() << endl;
        return -1;
    }
    catch (exception& e)
    {
        err_msg(e.what());
        return -1;
    }
    catch (...)
    {
        err_msg("undefined error");
        return -1;
    }
   
    return 0;
}
