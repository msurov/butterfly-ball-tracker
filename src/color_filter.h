#pragma once

#include <opencv2/opencv.hpp>
#include <array>
#include <cppmisc/json.h>
#include "json_cvmat.h"
#include "cv_helpers.h"


class ColorFilter
{
private:
    cv::Vec3i   _255_p0;
    cv::Vec3i   _1024_v;
    int         lim11;
    int         lim12;
    int         lim2;

public:
    ColorFilter(cv::Vec6d const& axis_coefs, cv::Vec2d const& tdiap, double const& radius);
    ColorFilter();
    ~ColorFilter();

    void init(cv::Vec6d const& axis_coefs, cv::Vec2d const& tdiap, double const& radius);
    int init(Json::Value const& jsoncfg);

    void process_bggr(cv::Mat const& bayer, cv::Mat& mask, int scale_factor = 2);
    void process_bgr(cv::Mat const& bgr, cv::Mat& mask, int scale_factor = 1);
};
