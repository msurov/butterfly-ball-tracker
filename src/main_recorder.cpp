#include <opencv2/opencv.hpp>

#include <functional>

#include <cppmisc/serializer.h>
#include <cppmisc/traces.h>
#include <cppmisc/signals.h>
#include <cppmisc/argparse.h>
#include <cppmisc/timing.h>

#include "camera.h"
#include "image_saver.h"


using namespace std;
using namespace cv;


class Saver
{
private:
    std::string _dir;
    std::string _format;
    int _nframes;
    int64_t _t_prev;
    int64_t _period;
    ImageSaver _saver;

public:
    Saver(Json::Value const& cfg)
    {
        auto const& savercfg = json_get<Json::Value>(cfg, "saver");
        _dir = json_get<string>(savercfg, "dir");
        _format = json_get<string>(savercfg, "format");
        _nframes = json_get<int>(savercfg, "nframes");
        _t_prev = 0;
        _period = int64_t(1e+6 / json_get<double>(savercfg, "rate"));
    }

    bool append_frame(Mat const& raw)
    {
        if (_nframes <= 0)
            return false;

        int64_t t = epoch_usec();
        if (t < _t_prev + _period)
            return true;
        _t_prev = t;

        auto const& name = format(_dir, "/", t, _format);
        _saver.push(name, raw, "bggr");

        -- _nframes;
        return _nframes > 0;
    }
};


int run_recorder(Json::Value const& cfg)
{
    // Ctrl+C handler
    bool stopped = false;
    auto f = [&stopped]() { stopped = true; };
    SysSignals::instance().set_sigint_handler(f);
    SysSignals::instance().set_sigterm_handler(f);

    Saver saver(cfg);
    auto cam = create_camera(cfg);

    auto cam_handler = [&saver,&stopped](uint8_t const* data, int Nx, int Ny, int64_t frame_timestamp) {
        Mat raw(Ny, Nx, CV_8U, (void*)data);
        bool b = saver.append_frame(raw);
        if (!b)
            stopped = true;
    };

    cam->set_handler(cam_handler);
    cam->run();

    while (!stopped)
        sleep_usec(1e+6);

    return 0;
}

int main(int argc, char const* argv[])
{
    Arguments args({
        Argument("-c", "config", "path to the butterfly robot config file", "", ArgumentsCount::One)
    });

    try
    {
        auto p = args.parse(argc, argv);
        string configpath = p["config"];
        auto cfg = json_load(configpath);
        run_recorder(cfg);
    }
    catch (invalid_argument& e)
    {
        err_msg(e.what());
        cout << args.help_message() << endl;
        return -1;
    }
    catch (exception& e)
    {
        err_msg(e.what());
        return -1;
    }
   
    return 0;
}
