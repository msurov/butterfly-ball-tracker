#include <tuple>
#include "color_filter.h"
#include <cppmisc/traces.h>
#include "cv_helpers.h"
#include "mathutils.h"


using namespace std;
using namespace cv;


/*
 * Color Filter
 */

ColorFilter::ColorFilter(Vec6d const& cyl_axis, Vec2d const& cyl_diap, double const& cyl_r)
{
    init(cyl_axis, cyl_diap, cyl_r);
}

ColorFilter::ColorFilter()
{
    _255_p0 = 0;
    _1024_v = 0;
    lim11 = 0;
    lim12 = 0;
    lim2 = 0;
}

ColorFilter::~ColorFilter()
{
}

template <typename T, int n>
T norm(Vec<T, n> const& v)
{
    return sqrt(v.dot(v));
}

template <typename T, std::size_t n>
Vec<T, int(n)> array2vec(array<T, n> const& arr)
{
    Vec<T, int(n)> v;

    for (int i = 0; i < int(n); ++i)
    {
        v[i] = arr[i];
    }

    return v;
}

void ColorFilter::init(Vec6d const& cyl_axis, Vec2d const& cyl_diap, double const& cyl_r)
{
    Vec3d v = Vec3d(cyl_axis[0], cyl_axis[1], cyl_axis[2]);
    Vec3d p0 = Vec3d(cyl_axis[3], cyl_axis[4], cyl_axis[5]);

    double k = 1.f / norm(v);

    v = k * v;
    p0 = k * p0;
    double _radius = k * cyl_r;

    lim11 = lround(1024. * 255. * cyl_diap[0]);
    lim12 = lround(1024. * 255. * cyl_diap[1]);
    lim2 = lround(square(255. * _radius));
    _255_p0 = Vec3i(255. * p0 + Vec3d(0.5, 0.5, 0.5));
    _1024_v = Vec3i(1024. * v + Vec3d(0.5, 0.5, 0.5));
}

int ColorFilter::init(Json::Value const& jsoncfg)
{
    auto const& filter = json_get(jsoncfg, "color_filter");
    Vec6d axis = json_get<Vec6d>(filter, "axis");
    Vec2d diap = json_get<Vec2d>(filter, "diap");
    double radius = json_get<double>(filter, "radius");
    init(axis, diap, radius);
    return 0;
}

void ColorFilter::process_bggr(Mat const& bggr, Mat& mask, int scale_factor)
{
    assert((scale_factor % 2) == 0);
    assert(scale_factor > 1);

    int const Nx = bggr.cols / scale_factor;
    int const Ny = bggr.rows / scale_factor;

    mask.create(Ny, Nx, CV_8U);

    for (int y = 0; y < Ny; ++y)
    {
        uchar* pmsk = mask.ptr<uchar>(y);
        uchar const* row1 = bggr.ptr<uchar>(y * scale_factor);
        uchar const* row2 = bggr.ptr<uchar>(y * scale_factor + 1);

        for (int x = 0; x < Nx; ++x)
        {
            uchar b = row1[x * scale_factor];
            uchar g = row1[x * scale_factor + 1];
            uchar r = row2[x * scale_factor + 1];

            Vec3i p(b, g, r);
            auto const& d255 = p - _255_p0;

            int v1 = _1024_v.dot(d255);
            int v2 = d255.dot(d255) - square(_1024_v.dot(d255) >> 10);
            pmsk[x] = in_diap(v1, lim11, lim12) && v2 < lim2;
        }
    }
}

void ColorFilter::process_bgr(Mat const& bgr, Mat& mask, int scale_factor)
{
    int const Nx = bgr.cols / scale_factor;
    int const Ny = bgr.rows / scale_factor;

    mask.create(Ny, Nx, CV_8U);

    for (int y = 0; y < Ny; ++y)
    {
        int y1 = y * scale_factor;
        Vec3b const* psrc = bgr.ptr<Vec3b>(y1);
        uchar* pmsk = mask.ptr<uchar>(y);

        for (int x = 0; x < Nx; ++x)
        {
            int const x1 = x * scale_factor;
            Vec3i p = psrc[x1];
            auto const& d255 = p - _255_p0;

            int v1 = _1024_v.dot(d255);
            int v2 = d255.dot(d255) - square(v1 >> 10);
            pmsk[x] = in_diap(v1, lim11, lim12) && v2 < lim2;
        }
    }
}

