#include "camera.h"

#include <pylon/PylonIncludes.h>
#include <pylon/PylonUtilityIncludes.h>

#include <cppmisc/traces.h>


using namespace Pylon;
using namespace std;

class BaslerCamera;
typedef std::shared_ptr<BaslerCamera> BaslerCameraPtr;


class CImageHandler : public CImageEventHandler
{
private:
    CameraCallback  _cb;

public:
    void set_user_handler(CameraCallback const& cb)
    {
        _cb = cb;
    }

    virtual void OnImageGrabbed(CInstantCamera& camera, CGrabResultPtr const& ptrGrabResult)
    {
        if (ptrGrabResult->GrabSucceeded())
        {
            if (_cb)
            {
                uint8_t const* data = (uint8_t*) ptrGrabResult->GetBuffer();
                int const Nx = ptrGrabResult->GetWidth();
                int const Ny = ptrGrabResult->GetHeight();
                uint64_t ts = ptrGrabResult->GetTimeStamp() / 1000;
                _cb(data, Nx, Ny, ts);
            }
        }
        else
        {
            err_msg(ptrGrabResult->GetErrorCode(), " ", std::string(ptrGrabResult->GetErrorDescription()));
        }
   }

    virtual void OnImagesSkipped(CInstantCamera& camera, size_t countOfSkippedImages)
    {
        warn_msg(countOfSkippedImages, " frames skipped");
    }
};


class BaslerCamera : public ICamera
{
private:
    unique_ptr<CInstantCamera> _camera;
    unique_ptr<CImageHandler> _camera_handler;

public:
    BaslerCamera()
    {
        PylonInitialize();

        try
        {
            _camera.reset(new CInstantCamera(CTlFactory::GetInstance().CreateFirstDevice()));
            _camera->MaxNumBuffer = 5;
            _camera->Open();
            _camera_handler.reset(new CImageHandler);
            _camera->RegisterImageEventHandler(_camera_handler.get(), RegistrationMode_Append, Cleanup_None);
        }
        catch (const GenericException &e)
        {
            throw_runtime_error("camera error: ", std::string(e.GetDescription()));
        }
    }

    ~BaslerCamera()
    {
        stop();
        _camera->DeregisterImageEventHandler(_camera_handler.get());
        _camera_handler.reset(nullptr);
        _camera.reset(nullptr);
        PylonTerminate();
    }

    void load(string const& config_filename)
    {
        if (!config_filename.empty())
            CFeaturePersistence::Load(config_filename.c_str(), &_camera->GetNodeMap(), true);
    }

    void set_handler(CameraCallback const& handler) override
    {
        _camera_handler->set_user_handler(handler);
    }

    void run() override
    {
        _camera->StartGrabbing(GrabStrategy_LatestImageOnly, GrabLoop_ProvidedByInstantCamera);
    }

    void stop() override
    {
        _camera->StopGrabbing();
    }

    string description() override
    {
        return _camera->GetDeviceInfo().GetModelName().c_str();
    }

    string image_type() override
    {
    	return "BayerBGGR";
    }
};

ICameraPtr create_basler_camera(Json::Value const& cfg)
{
    auto const& config_path = json_get<string>(cfg, "config_path");
    BaslerCameraPtr cam = make_shared<BaslerCamera>();
    cam->load(config_path);
    return cam;
}
