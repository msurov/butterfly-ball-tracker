
#include "imgdump.h"

using namespace std;
using namespace cv;

inline std::weak_ptr<ImgDumpManager> instance;


ImgDumpManager::ImgDumpManager() : _path(), _active(false) {}

void ImgDumpManager::init(Json::Value const& cfg)
{
    auto logger = json_get(cfg, "imgdump");
    if (json_has(logger, "img_dump_dir"))
    {
        _path = json_get<std::string>(logger, "img_dump_dir");
        _active = true;
    }
    else
    {
        _active = false;
    }
}

void ImgDumpManager::doit(std::string const& name, cv::Mat const& img, std::string const& type)
{
    if (!_active)
        return;
    _saver.push(_path + "/" + name, img, type);
}

ImgDumpManagerPtr imgdump_init(Json::Value const& cfg)
{
    if (instance.use_count() > 0)
        throw_runtime_error("ImgDumpManager is already initialized");
    ImgDumpManagerPtr ptr(new ImgDumpManager());
    ptr->init(cfg);
    instance = ptr;
    return ptr;
}

void imgdump(string const& name, Mat const& img, string const& type)
{
    if (auto ptr = instance.lock())
        ptr->doit(name, img, type);
}
