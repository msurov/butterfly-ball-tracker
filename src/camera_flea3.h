#include <FlyCapture2.h>
#include <opencv2/opencv.hpp>
#include <stdint.h>
#include <functional>


class camera
{
public:
    typedef std::function<void(cv::Mat const&, uint32_t)> image_handler_t;
    friend void callback(FlyCapture2::Image* raw, void const* arg);

private:
    FlyCapture2::Camera     m_camera;
    int                     m_width;
    int                     m_height;
    image_handler_t         m_handler;
    uint32_t                m_prev_ts;
    uint32_t                m_prev_t;

private:
    void connect(FlyCapture2::PGRGuid& guid);
    void set_mode7();
    void get_max_resolution(int& width, int& height);

    void callback(FlyCapture2::Image* raw);

public:
    camera(image_handler_t const& handler);
    ~camera();

    void run();
    void stop();
    double fps();
    void printf_info();
};
