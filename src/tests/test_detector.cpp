#include <cppmisc/argparse.h>
#include <cppmisc/traces.h>
#include <cppmisc/files.h>
#include <cppmisc/timing.h>

#include "../detector.h"
#include "../imgdump.h"

using namespace std;
using namespace cv;


vector<string> collect_names(vector<string> const& masklist)
{
    vector<string> files;

    for (auto const& mask : masklist)
    {
        auto const& v = get_files(mask);
        files.insert(files.end(), v.begin(), v.end());
    }

    return files;
}

void process_samples(Json::Value const& cfg, vector<string> const& masklist)
{
    auto imgdump = imgdump_init(cfg);

    FindBall find_ball;
    find_ball.init(cfg);

    vector<string> files = collect_names(masklist);

    for (int i = 0; i < files.size(); ++i)
    {
        string imgpath = files[i];
        string name = get<1>(splitname(imgpath));
        string basename = get<0>(splitext(name));
        info_msg("processing ", name);

        Mat img = read_raw(imgpath);
        info_msg("input size: ", img.size());

        if (img.empty())
        {
            err_msg("can't read ", name);
            continue;
        }

        Vec2d pos;
        int64_t t = epoch_usec();
        find_ball.process_raw(img, pos);
        t = epoch_usec() - t;
        info_msg("processed in ", t);

        info_msg(basename, ": ", pos);

        tie(name, ignore) = splitext(imgpath);
        find_ball.dump(basename + "_");
    }
}

int main(int argc, char const* argv[])
{
    Arguments args({
        Argument("-c", "config", "path to the butterfly robot config file", "", ArgumentsCount::One)
    });

    try
    {
        auto p = args.parse(argc, argv);
        string configpath = p["config"];
        auto cfg = json_load(configpath);

        if (json_has(cfg, "logger"))
            traces::init(json_get(cfg, "logger"));

        vector<string> inputs;

        if (!p.has("rest"))
        {
            err_msg("no input images to process");
            return -1;
        }

        inputs.reserve(p.size("rest"));
        for (int i = 0; i < p.size("rest"); ++ i)
            inputs.push_back(p.get("rest", i));

        process_samples(cfg, inputs);
    }
    catch (invalid_argument& e)
    {
        err_msg(e.what());
        cout << args.help_message() << endl;
        return -1;
    }
    catch (exception& e)
    {
        err_msg(e.what());
        return -1;
    }
    catch (...)
    {
        err_msg("unknown error");
        return -1;
    }
   
    return 0;
}
