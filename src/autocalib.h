#pragma once

#include <opencv2/opencv.hpp>
#include <cmath>
#include <cppmisc/traces.h>


inline float norm(cv::Point2f const& p)
{
    return sqrtf(p.x * p.x + p.y * p.y);
}

inline cv::Point2i round(cv::Point2f const& p)
{
    return cv::Point2i(lroundf(p.x), lroundf(p.y));
}

inline cv::Mat downscale(cv::Mat const& im, int resolution)
{
    cv::Mat dst = im;

    while (dst.total() > resolution)
    {
        cv::Mat tmp;
        cv::pyrDown(dst, tmp);
        dst = tmp;
    }

    return dst;
}

inline void upscale_pts(std::vector<cv::Point2f>& pts, float n)
{
    for (auto& p : pts)
        p *= n;
}

inline float dist(cv::Point2f const& p1, cv::Point2f const& p2)
{
    cv::Point2f d = p2 - p1;
    return sqrtf(d.dot(d));
}

inline float cross(cv::Vec2f const& a, cv::Vec2f const& b)
{
    return a[0] * b[1] - a[1] * b[0];
}

inline float norm(cv::Vec2f const& v)
{
    return sqrtf(v.dot(v));
}

inline float vec_angle(cv::Vec2f const& a, cv::Vec2f const& b)
{
    return cross(a, b) / (norm(a) * norm(b));
}

template <typename Arr>
inline typename Arr::value_type max_elem(Arr const& arr)
{
    assert(arr.size() > 0);

    typename Arr::value_type m = *arr.begin();

    for (auto const& a : arr)
        m = std::max(m, a);

    return m;
}

template <typename Arr>
inline typename Arr::value_type min_elem(Arr const& arr)
{
    assert(arr.size() > 0);

    typename Arr::value_type m = *arr.begin();

    for (auto const& a : arr)
        m = std::min(m, a);

    return m;
}

static cv::Size2i get_refine_size(std::vector<cv::Point2f> const& corners, cv::Size2i const& pattern_shape)
{
    const int Ny = pattern_shape.height;
    const int Nx = pattern_shape.width;

    assert(corners.size() == Nx * Ny);

    float mindist = std::numeric_limits<float>::max();

    for (int y = 1; y < Ny; ++ y)
    {
        for (int x = 1; x < Nx; ++ x)
        {
            auto const& p1 = corners[y * Nx + x];
            auto const& p2 = corners[y * Nx + x - 1];
            auto const& p3 = corners[(y - 1) * Nx + x];
            auto const& p4 = corners[(y - 1) * Nx + x - 1];
            mindist = std::min(mindist, dist(p1, p2));
            mindist = std::min(mindist, dist(p1, p3));
            mindist = std::min(mindist, dist(p1, p4));
        }
    }

    int d = int(mindist / 3.f + 0.5f);
    return cv::Size2i(d, d);
}

namespace cv {
    typedef Matx<uchar,3,3> Matx33b;
};


struct PointCnt {
    cv::Point2f pt;
    int cnt;

    PointCnt() : pt(0,0), cnt(0) {}
    ~PointCnt() {}
};

struct Chessboard {
    std::vector<cv::Point2f> corners;
    cv::Size2i shape;

    Chessboard() : corners(), shape(0,0) {}
    ~Chessboard() {}
};

struct Contour {
    std::vector<cv::Point2f> verts;
};

inline void chessboard_contour(Chessboard const& cb, Contour& contour)
{
    assert(cb.corners.size() == cb.shape.width * cb.shape.height);

    contour.verts = {
        cb.corners[0],
        cb.corners[cb.shape.width - 1], 
        cb.corners[(cb.shape.height - 1) * cb.shape.width + cb.shape.width - 1],
        cb.corners[(cb.shape.height - 1) * cb.shape.width],
    };    
}

inline cv::Matx33f chessboard_transform(Chessboard const& cb1, Chessboard const& cb2)
{
    Contour cont1, cont2;
    chessboard_contour(cb1, cont1);
    chessboard_contour(cb2, cont2);
    return cv::findHomography(cont1.verts, cont2.verts);
}

inline float chessboard_dist(Chessboard const& cb1, Chessboard const& cb2)
{
    Contour cont1, cont2;
    chessboard_contour(cb1, cont1);
    chessboard_contour(cb2, cont2);
    auto const& dif = {
        dist(cont1.verts[0], cont2.verts[0]),
        dist(cont1.verts[1], cont2.verts[1]),
        dist(cont1.verts[2], cont2.verts[2]),
        dist(cont1.verts[3], cont2.verts[3])
    };
    return max_elem(dif);
}

inline void draw_chessboard(cv::Mat& plot, Chessboard const& cb)
{
    cv::drawChessboardCorners(plot, cb.shape, cb.corners, true);
}

inline void draw_chessboard_pattern(cv::Mat& plot, cv::Size2i const& shape)
{
    const int Ny = shape.height;
    const int Nx = shape.width;

    plot = 255;
    float d = std::min(float(plot.cols - 2) / Nx, float(plot.rows - 2) / Ny);
    float pat_w = Nx * d;
    float pat_h = Ny * d;
    float cx = plot.cols / 2.f - 0.5f;
    float cy = plot.rows / 2.f - 0.5f;
    float ox = cx - pat_w / 2.f;
    float oy = cy - pat_h / 2.f;

    for (int y = 0; y < Ny; ++y)
    {
        for (int x = y % 2; x < Nx; x += 2)
        {
            cv::Point2f p1(ox + x * d + 0.5f, oy + y * d + 0.5f);
            cv::Point2f p2(ox + x * d + d - 0.5f, oy + y * d + d - 0.5f);
            cv::rectangle(plot, round(p1), round(p2), cv::Scalar(0), -1, CV_AA);
        }
    }
}

inline void draw_line(cv::Mat& plot, cv::Point2f const& p1, cv::Point2f const& p2, cv::Scalar const& color)
{
    int n = int(norm(p2 - p1) + 0.5f);
    n = std::max(n, 1);
    cv::Size2i shape = plot.size();

    for (int i = 0; i <= n; ++ i)
    {
        cv::Point2i p = round((p2 - p1) * i / n + p1);
        if (p.x < 0 || p.x >= shape.width ||
            p.y < 0 || p.y >= shape.height)
        {
            continue;
        }
        plot.at<uchar>(p.y, p.x) = color[0];
    }
}

inline void draw_contour(cv::Mat& plot, Contour const& contour, cv::Scalar const& color)
{
    int n = contour.verts.size();
    for (int i = 0; i < n; ++ i)
    {
        draw_line(plot, contour.verts[i], contour.verts[(i + 1) % n], color);
    }
}

inline bool inside_contour(cv::Point2f const& p, Contour const& contour)
{
    const int n = contour.verts.size();
    float angle = 0;

    for (int i = 0; i < n; ++ i)
    {
        cv::Vec2f a = contour.verts[i] - p;
        cv::Vec2f b = contour.verts[(i + 1) % n] - p;
        angle += vec_angle(a,b);
    }

    int nrevs = lroundf(fabs(angle) / (2 * M_PI));
    return nrevs != 0;
}

template <typename A, typename B>
inline void convert(std::vector<A> const& in, std::vector<B>& out)
{
    const int N = in.size();
    out.resize(N);

    for (int i = 0; i < N; ++ i)
        out[i] = in[i];
}

float pts_max_dist(std::vector<cv::Point2f> const& a, std::vector<cv::Point2f> const& b)
{
    const int N = a.size();
    assert(b.size() == N);
    assert(N > 0);

    float d = 0;

    for (int i = 0; i < N; ++ i)
    {
        d = std::max(d, dist(a[i], b[i]));
    }

    return d;
}

struct CameraIntrinsics
{
    cv::Matx33f K;
    cv::Size2i resolution; 
    std::vector<float> distortion;

    CameraIntrinsics()
    {
        K = cv::Matx33f::zeros();
        resolution = {0,0};
    }
};


class AutoCalib
{
private:
    cv::Size2i _pattern_shape;
    int _detect_resolution;
    std::vector<Chessboard> _chessboards;
    CameraIntrinsics _intrinsics;
    cv::Matx33b _regions; 
    std::vector<PointCnt> _ref_pts;
    float _dist_thresh;
    float _ref_max_cnt;
    cv::Size2f _cb_cell_sz;


    bool initialized()
    {
        return _intrinsics.resolution.area() > 0;
    }

    void init(cv::Mat const& im)
    {
        _intrinsics.resolution = im.size();
        const float Nx = _intrinsics.resolution.width;
        const float Ny = _intrinsics.resolution.height;

        _intrinsics.K = cv::Matx33f(
            Nx/2,    0, Nx/2 - 0.5f,
               0, Nx/2, Ny/2 - 0.5f, 
               0,    0,           1
        );
        _dist_thresh = std::min(Nx, Ny) / 20.f;
        _ref_max_cnt = 3;

        _ref_pts.resize(9);
        _ref_pts[0].pt = cv::Point2f(2.f * Nx / 6, 2.f * Ny / 6);
        _ref_pts[1].pt = cv::Point2f(3.f * Nx / 6, 2.f * Ny / 6);
        _ref_pts[2].pt = cv::Point2f(4.f * Nx / 6, 2.f * Ny / 6);
        _ref_pts[3].pt = cv::Point2f(2.f * Nx / 6, 3.f * Ny / 6);
        _ref_pts[4].pt = cv::Point2f(3.f * Nx / 6, 3.f * Ny / 6);
        _ref_pts[5].pt = cv::Point2f(4.f * Nx / 6, 3.f * Ny / 6);
        _ref_pts[6].pt = cv::Point2f(2.f * Nx / 6, 4.f * Ny / 6);
        _ref_pts[7].pt = cv::Point2f(3.f * Nx / 6, 4.f * Ny / 6);
        _ref_pts[8].pt = cv::Point2f(4.f * Nx / 6, 4.f * Ny / 6);
    }

    std::vector<cv::Vec3f> chessboard_points()
    {
        std::vector<cv::Vec3f> cb_obj_pts(_pattern_shape.area());

        for (int y = 0; y < _pattern_shape.height; ++y)
        {
            for (int x = 0; x < _pattern_shape.width; ++x)
            {
                cv::Vec3f& p = cb_obj_pts[y * _pattern_shape.width + x];
                p[0] = _cb_cell_sz.width * x;
                p[1] = _cb_cell_sz.height * y;
                p[2] = 0;
            }
        }

        return cb_obj_pts;
    }


public:
    AutoCalib(cv::Size2i const& pattern_shape, cv::Size2f const& cb_cell_sz)
    {
        _pattern_shape = cv::Size(pattern_shape.width - 1, pattern_shape.height - 1);
        _detect_resolution = 2*1024*1024;
        _regions = 0;
        _cb_cell_sz = cb_cell_sz;
    }

    bool calibrate()
    {
        int flags = cv::CALIB_ZERO_TANGENT_DIST;

        const int N = _chessboards.size();
        const auto& cb_obj_pts = chessboard_points();

        std::vector<std::vector<cv::Vec3f>> obj_pts(N);
        std::vector<std::vector<cv::Vec2f>> im_pts(N);

        for (int i = 0; i < N; ++ i)
        {
            obj_pts[i] = cb_obj_pts;
            convert(_chessboards[i].corners, im_pts[i]);
        }

        std::vector<float> distortion;
        cv::Matx33f K;

        // cv::calibrateCamera(obj_pts, im_pts, _intrinsics.resolution, K, distortion, rvecs, tvecs, flags);
        cv::calibrateCamera(obj_pts, im_pts, _intrinsics.resolution, K, distortion, cv::noArray(), cv::noArray(), flags);

        _intrinsics.K = K;
        _intrinsics.distortion = distortion;
        return true;
    }

    inline CameraIntrinsics const& intrinsics()
    {
        return _intrinsics;
    }

    std::vector<float> tolerance()
    {
        const int N = _chessboards.size();
        std::vector<float> distances(N); 

        std::vector<cv::Point3f> obj_pts;
        convert(chessboard_points(), obj_pts);

        for (int i = 0; i < N; ++ i)
        {
            // cv::Vec3f rvec, tvec;
            cv::Mat rvec, tvec;
            std::vector<cv::Point2f> const& impts1 = _chessboards[i].corners;
            std::vector<cv::Point2f> impts2;

            cv::solvePnP(obj_pts, impts1, _intrinsics.K, _intrinsics.distortion, rvec, tvec);
            cv::projectPoints(obj_pts, rvec, tvec, _intrinsics.K, _intrinsics.distortion, impts2);
            float d = pts_max_dist(impts1, impts2);
            distances[i] = d;
        }

        return distances;
    }

    bool detect_chessboard(cv::Mat const& im, Chessboard& cb)
    {
        cv::Mat downscaled = downscale(im, _detect_resolution);
        cv::Mat normalized;
        cv::equalizeHist(downscaled, normalized);
        bool b = cv::findChessboardCorners(normalized, _pattern_shape, cb.corners);
        if (!b)
            return false;
        if (cb.corners.size() == 0)
            return false;

        float scale = 1.f * im.cols / downscaled.cols;
        upscale_pts(cb.corners, scale);
        cv::Size2i refine_size = get_refine_size(cb.corners, _pattern_shape);
        cv::TermCriteria criteria;
        criteria.type = cv::TermCriteria::EPS + cv::TermCriteria::MAX_ITER;
        criteria.epsilon = 1e-3;
        criteria.maxCount = 30;
        cv::cornerSubPix(im, cb.corners, refine_size, cv::Size2i(), criteria);
        cb.shape = _pattern_shape;
        return true;
    }

    bool ref_pts_filled() const
    {
        for (auto const& rp : _ref_pts)
        {
            if (rp.cnt < _ref_max_cnt)
                return false;
        }

        return true;
    }

    bool done() const
    {
        return ref_pts_filled();
    }

    std::tuple<bool,Chessboard> append_frame(cv::Mat const& gray)
    {
        try
        {
            assert(gray.type() == CV_8UC1);
            assert(gray.total() > 0);

            if (!initialized())
                init(gray);

            Chessboard cb;
            bool b = detect_chessboard(gray, cb);
            if (!b)
            {
                dbg_msg("can't detect, skipped");
                return std::make_tuple(false, cb);
            }

            if (_chessboards.size() > 0)
            {
                auto const& latest = *_chessboards.rbegin();
                float d = chessboard_dist(latest, cb);
                if (d < _dist_thresh)
                {
                    dbg_msg("too close, skipped");
                    return std::make_tuple(false, cb);
                }
            }

            Contour contour;
            chessboard_contour(cb, contour);
            bool need = false;

            for (auto& rp : _ref_pts)
            {
                if (rp.cnt >= _ref_max_cnt)
                    continue;

                if (inside_contour(rp.pt, contour))
                {
                    need = true;
                    ++ rp.cnt;
                }
            }

            if (need)
            {
                _chessboards.emplace_back(cb);
                return std::make_tuple(true, cb);
            }

            return std::make_tuple(false, cb);
        }
        catch (std::exception& e)
        {
            err_msg("failed: ", e.what());
        }
        catch (...)
        {
            err_msg("failed: unknown");
        }

        return std::make_tuple(false, Chessboard());
    }
};
