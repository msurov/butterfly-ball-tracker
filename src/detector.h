#pragma once

#include <vector>
#include <opencv2/opencv.hpp>

#include "color_filter.h"
#include <cppmisc/json.h>


class LocateObject
{
private:
    std::vector<int> m_histx;
    std::vector<int> m_histy;
    int m_object_size;

public:
    LocateObject() : m_object_size(0) {}
    void init(int object_size) { m_object_size = object_size; }

    cv::Rect process(cv::Mat const& mask);
    void dump(std::string const& prefix);
};

class FindBall
{
private:
    LocateObject m_locate_coarse;
    ColorFilter m_color_filter;
    cv::Matx33d m_A;
    cv::Matx33d m_A_inv;
    cv::Matx33d m_P_inv;
    std::vector<double> m_distortion;

    cv::Mat m_mask_ds1;
    cv::Mat m_mask_ds;
    cv::Rect m_region_ds;
    cv::Rect m_region_orig;
    cv::Mat m_cropped_bgr;
    cv::Mat m_mask_cr1;
    cv::Mat m_mask_cr;

    int m_ball_radius_px;
    int m_scale;
    int m_expect_inliers;

    std::vector<cv::Point2i> m_edge_points;
    std::vector<bool> m_plane_inliers;

    cv::Vec3d m_plane_rns;
    cv::Vec3d m_plane_lsq;

    double m_focus;
    double m_ball_angle_size;
    double m_fit_ransac_thresh_angle;
    int m_fit_ransac_thresh_px;

    void clear();
    void draw_inliers(cv::Mat& plot);

public:
    void init(Json::Value const& cfg);
    bool process_raw(cv::Mat const& bggr, cv::Vec2d& position);

    void dump(std::string const& prefix);
};
