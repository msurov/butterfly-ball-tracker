#include <tuple>
#include <cppmisc/traces.h>
#include <cppmisc/json.h>
#include "detector.h"
#include "cv_helpers.h"
#include "mathutils.h"
#include "imgdump.h"

using namespace std;
using namespace cv;

/*
 * Object region dtection -- coarse analysis
 */
void histograms(Mat const& mask, vector<int>& histx, vector<int>& histy)
{
    int const Nx = mask.cols;
    int const Ny = mask.rows;

    histx.resize(Nx);
    std::fill(histx.begin(), histx.end(), 0);
    histy.resize(Ny);
    std::fill(histy.begin(), histy.end(), 0);

    int* phistx = histx.data();

    for (int y = 0; y < Ny; ++y)
    {
        uchar const* pmsk = mask.ptr<uchar>(y);
        int& vhisty = histy[y];

        for (int x = 0; x < Nx; ++x)
        {
            uchar val = pmsk[x];
            phistx[x] += val;
            vhisty += val;
        }
    }
}

inline int get_hist_total(vector<int> const& hist)
{
    int total = 0;
    for (auto const& h : hist)
        total += h;
    return total;
}

inline tuple<int, int> hist_locate_obj(vector<int> const& hist, int total, int thresh_pcnt)
{
    int threshold = total * thresh_pcnt / 100;

    int i1 = 0;
    for (int sum = 0; sum < threshold; ++i1)
        sum += hist[i1];

    int i2 = (int)hist.size() - 1;
    for (int sum = 0; sum < threshold; --i2)
        sum += hist[i2];

    return make_tuple(i1, i2);
}

inline int get_hist_median(vector<int> const& hist, int total)
{
    int threshold = total / 2;
    int i = 0;
    for (int sum = 0; sum < threshold; ++i)
        sum += hist[i];
    return i;
}

inline int diff(tuple<int, int> const& t)
{
    return get<1>(t) -get<0>(t);
}

inline tuple<int, int> clarify_edge(vector<int> const& hist, tuple<int, int> const& lims0, int threshold)
{
    int x1 = get<0>(lims0);
    for (; x1 > 0 && hist[x1] > threshold; --x1) {}

    int x2 = get<1>(lims0);
    for (; x2 < (int)hist.size() && hist[x2] > threshold; ++x2) {}

    return make_tuple(x1, x2);
}

inline tuple<int, int> widen(vector<int> const& hist, tuple<int, int> const& lims, int d)
{
    return make_tuple(
        max(get<0>(lims) -d, 0),
        min(get<1>(lims) +d, (int)hist.size() - 1)
        );
}

int align_down(int v, int r)
{
    return v - (v % r);
}

int align_up(int v, int r)
{
    return align_down(v + r - 1, r);
}

inline Rect align_region(Rect const& r, int round_val)
{
    int x1 = align_down(r.tl().x, round_val);
    int y1 = align_down(r.tl().y, round_val);
    int x2 = align_up(r.br().x, round_val);
    int y2 = align_up(r.br().y, round_val);
    return Rect(Point2i(x1, y1), Point2i(x2, y2));
}

inline Rect upscale_region(Rect const& r, int scale)
{
    return Rect(r.tl() * scale, r.br() * scale);
}

/*
 * Edge detection
 */

// checks if horizontal line with coordinate y
// crosses a circle with radius r and center (cx,cy)
// returns x-coordinates of intersection points: x1, x2 or (-1,-1)
tuple<int, int> line_crosses_circle(int r, int cx, int cy, int y)
{
    int d = square(r) - square(y - cy);
    if (d < 0)
        return make_tuple(-1, -1);
    int x1 = cx - int(sqrt(d) + 0.5);
    int x2 = cx + int(sqrt(d) + 0.5);

    return make_tuple(x1, x2);
}

tuple<Range, Range> get_scan_regions(int min_radius, Size const& sz, int y)
{
    int y1 = 0;
    int y2 = y1 + sz.height;
    int x1 = 0;
    int x2 = x1 + sz.width;

    int xl1;

    if (y >= y1 && y < y1 + min_radius)
    {
        auto cr = line_crosses_circle(min_radius, x1 + min_radius, y1 + min_radius, y);
        xl1 = get<0>(cr);
    }
    else if (y >= y1 + min_radius && y < y2 - min_radius)
    {
        xl1 = x1;
    }
    else if (y >= y2 - min_radius && y < y2)
    {
        auto cr = line_crosses_circle(min_radius, x1 + min_radius, y2 - min_radius, y);
        xl1 = get<0>(cr);
    }
    else
    {
        return make_tuple(Range(), Range());
    }

    int xl2;

    if (y >= y1 && y < y2 - 2 * min_radius)
    {
        xl2 = x2 - min_radius;
    }
    else if (y >= y2 - 2 * min_radius && y < (y2 + y1) / 2)
    {
        auto cr = line_crosses_circle(min_radius, x2 - min_radius, y2 - min_radius, y);
        xl2 = get<0>(cr);
    }
    else if (y >= (y2 + y1) / 2 && y < y1 + 2 * min_radius)
    {
        auto cr = line_crosses_circle(min_radius, x2 - min_radius, y1 + min_radius, y);
        xl2 = get<0>(cr);
    }
    else if (y >= y1 + 2 * min_radius && y < y2)
    {
        xl2 = x2 - min_radius;
    }
    else
    {
        return make_tuple(Range(), Range());
    }

    int xr1 = x2 - (xl2 - x1);
    int xr2 = x2 - (xl1 - x1);

    if (xl2 >= xr1)
        return make_tuple(Range(xl1, xr2), Range());

    return make_tuple(Range(xl1, xl2), Range(xr1, xr2));
}

Range clamp_range(Range const& range, int minval, int maxval)
{
    return Range(max(range.start, minval), min(range.end, maxval));
}

void extract_edge_points(Mat const& msk, int radius, vector<Point2i>& points)
{
    int const Nx = msk.cols;
    int const min_radius = radius * 9 / 10;
    int const y1 = 0;
    int const y2 = msk.rows;
    Size const sz = msk.size();

    points.clear();

    for (int y = y1; y < y2; ++y)
    {
        uchar const* pmsk0 = msk.ptr<uchar>(std::clamp(y - 1, 0, msk.rows - 1));
        uchar const* pmsk1 = msk.ptr<uchar>(std::clamp(y + 0, 0, msk.rows - 1));
        uchar const* pmsk2 = msk.ptr<uchar>(std::clamp(y + 1, 0, msk.rows - 1));

        Range r1, r2;
        tie(r1, r2) = get_scan_regions(min_radius, sz, y);

        if (!r1.empty())
        {
            Range r = clamp_range(r1, 1, Nx - 2);

            for (int x = r.start; x <= r.end; ++x)
            {
                uchar v = ~(pmsk0[x] & pmsk1[x - 1] & pmsk1[x + 1] & pmsk2[x]) & pmsk1[x];
                if (v)
                    points.push_back(Point2i(x, y));
            }
        }

        if (!r2.empty())
        {
            Range r = clamp_range(r2, 1, Nx - 2);

            for (int x = r.start; x <= r.end; ++x)
            {
                uchar v = ~(pmsk0[x] & pmsk1[x - 1] & pmsk1[x + 1] & pmsk2[x]) & pmsk1[x];
                if (v)
                    points.push_back(Point2i(x, y));
            }
        }
    }
}


/*
 * Refinment of ball position
 *
 * rays scanner
 */

static void get_filtered(vector<Point2i> const& src, vector<bool> const& mask, vector<Point2i>& filtered)
{
    filtered.clear();
    filtered.reserve(src.size());

    for (int i = 0; i < (int)src.size(); ++i)
    {
        if (mask[i])
            filtered.push_back(src[i]);
    }
}

inline Vec3d normalized(Vec3d const& v)
{
    return v / sqrt(v.dot(v));
}

// 
// input: masked array of 3d points 
// output: coefficients lx, ly, lz of line l
//  lx * x + ly * y + lz * z + 1 = 0
// 
static Vec3d fit_plane_lsq(vector<Vec3d> const& unit_vectors, vector<bool> const& mask)
{
    vector<Vec3d> _A;
    vector<double> _B;

    _A.reserve(unit_vectors.size());
    _B.reserve(unit_vectors.size());

    for (int i = 0; i < mask.size(); ++i)
    {
        if (mask[i])
        {
            _A.push_back(unit_vectors[i]);
            _B.push_back(-1.0f);
        }
    }

    Mat A((int)_A.size(), 3, CV_64F, _A.data());
    Mat B((int)_B.size(), 1, CV_64F, _B.data());
    Mat Z(3, 1, CV_64F);
    cv::solve(A, B, Z, DECOMP_SVD);
    Vec3f z = Z;

    return z;
}

inline Vec3d plane_by_3pts(Vec3d const& p1, Vec3d const& p2, Vec3d const& p3)
{
    Matx33d A(
        p1[0], p1[1], p1[2],
        p2[0], p2[1], p2[2],
        p3[0], p3[1], p3[2]
    );
    Vec3d B(
        -1.0, -1.0, -1.0
    );
    return A.inv() * B;
}

static Vec3d random_plane(vector<Vec3d> const& unit_vectors, double minang)
{
    int const N = (int)unit_vectors.size();
    int i1, i2, i3;

    i1 = rand() % N;

    double cosminang = cos(minang);

    for (int i = 0; i < N; ++i)
    {
        i2 = rand() % N;
        i3 = rand() % N;

        Vec3d const& v1 = unit_vectors[i1];
        Vec3d const& v2 = unit_vectors[i2];
        Vec3d const& v3 = unit_vectors[i3];

        if (v1.dot(v2) < cosminang && v2.dot(v3) < cosminang && v1.dot(v3) < cosminang)
        	return plane_by_3pts(v1, v2, v3);
    }

    return plane_by_3pts(unit_vectors[i1], unit_vectors[i2], unit_vectors[i3]);
}

static void get_plane_inliers(Vec3d const& plane, vector<Vec3d> const& unit_vectors, vector<bool>& mask, int& ninliers, double thresh)
{
    mask.resize(unit_vectors.size());
    ninliers = 0;

    for (int i = 0; i < unit_vectors.size(); ++i)
    {
        double err = plane.dot(unit_vectors[i]) + 1.0;
        bool b = fabs(err) < thresh;
        mask[i] = b;
        ninliers += b ? 1 : 0;
    }
}

static Vec3d fit_plane_ransac(vector<Vec3d> const& unit_vectors, vector<bool>& mask, double minang, double thresh)
{
    int iterations = 100;
    int ninliers;
    int maxinliers = 0;
    Vec3f bestplane;

    for (int i = 0; i < iterations; ++i)
    {
        Vec3d const& p = random_plane(unit_vectors, minang);
        get_plane_inliers(p, unit_vectors, mask, ninliers, thresh);
        if (ninliers > maxinliers)
        {
            bestplane = p;
            maxinliers = ninliers;
        }
    }

    dbg_msg("RANSAC ninliers: ", maxinliers, "; total ", unit_vectors.size());

    get_plane_inliers(bestplane, unit_vectors, mask, ninliers, thresh);
    return bestplane;
}

cv::Rect LocateObject::process(Mat const& mask)
{
    assert(m_object_size > 0);
    histograms(mask, m_histx, m_histy);

    int total = get_hist_total(m_histx);
    auto xlims = hist_locate_obj(m_histx, total, 5);
    auto ylims = hist_locate_obj(m_histy, total, 5);
    // dbg_msg("xlims 5%: ", xlims);
    // dbg_msg("ylims 5%: ", ylims);

    if (diff(xlims) > m_object_size * 11 / 10)
    {
        int x0 = get_hist_median(m_histx, total);
        xlims = make_tuple(x0, x0);
        // dbg_msg("xmed: ", x0);
    }

    if (diff(ylims) > m_object_size * 11 / 10)
    {
        int y0 = get_hist_median(m_histy, total);
        ylims = make_tuple(y0, y0);
        // dbg_msg("ymed: ", y0);
    }

    int thresh = max(m_object_size / 20, 2);

    xlims = clarify_edge(m_histx, xlims, thresh);
    ylims = clarify_edge(m_histy, ylims, thresh);
    // dbg_msg("xlims edge: ", xlims);
    // dbg_msg("ylims edge: ", ylims);

    xlims = widen(m_histx, xlims, (m_object_size + 19) / 20);
    ylims = widen(m_histy, ylims, (m_object_size + 19) / 20);
    // dbg_msg("xlims widen: ", xlims);
    // dbg_msg("ylims widen: ", ylims);

    if (
        !in_diap(diff(xlims), m_object_size * 9 / 10, 8 + m_object_size * 11 / 10) ||
        !in_diap(diff(ylims), m_object_size * 9 / 10, 8 + m_object_size * 11 / 10)
        )
    {
        return Rect(0, 0, 0, 0);
    }

    return Rect(get<0>(xlims), get<0>(ylims), diff(xlims) + 1, diff(ylims) + 1);
}

Mat draw_hist(vector<int> const& hist)
{
    auto b = maxval(hist);

    Mat plot(b + 1, (int)hist.size(), CV_8U, Scalar(0));

    for (int u = 0; u < hist.size(); ++u)
    {
        Rect r(u, b - hist[u] + 1, 1, hist[u]);
        plot(r) = 255;
    }

    return plot;
}

void LocateObject::dump(std::string const& prefix)
{
    auto plot = draw_hist(m_histx);
    imgdump(prefix + "_histx.png", plot);

    plot = draw_hist(m_histy);
    imgdump(prefix + "_histy.png", plot);
}


/*
 * FindBall
 */

void FindBall::init(Json::Value const& cfg)
{
    auto scaner = json_get(cfg, "scaner");

    m_ball_radius_px = json_get<int>(scaner, "ball_radius");
    m_color_filter.init(scaner);

    auto cam = json_get(scaner, "camera_parameters");
    json_get(cam, "A", m_A);

    Vec3d rv, tv;
    json_get(cam, "rv", rv);
    json_get(cam, "tv", tv);
    cv::Matx33d R;
    cv::Rodrigues(rv, R);

    cv::Matx33d P(
        R(0,0), R(0,1), tv(0), 
        R(1,0), R(1,1), tv(1),
        R(2,0), R(2,1), tv(2)
    );

    m_P_inv = P.inv();
    m_distortion = json_get<vector<double>>(cam, "distortion");

    m_scale = 8;
    m_locate_coarse.init(m_ball_radius_px * 2 / m_scale);

    m_focus = m_A(0, 0);
    m_ball_angle_size = m_ball_radius_px * 2 / m_focus;

    m_fit_ransac_thresh_px = 1;
    m_fit_ransac_thresh_angle = cos(m_ball_angle_size / 2) - cos(m_ball_angle_size / 2 + m_fit_ransac_thresh_px / m_focus);

    m_expect_inliers = int(2 * _PI * m_ball_radius_px);
}

void FindBall::clear()
{
}

bool FindBall::process_raw(Mat const& bggr, cv::Vec2d& pt)
{
    assert(bggr.total() > 0);
    assert(m_ball_radius_px > 0);

    //
    // find coarse
    //
    m_color_filter.process_bggr(bggr, m_mask_ds1, m_scale);
    BinMedian3x3(m_mask_ds1, m_mask_ds);

    m_region_ds = m_locate_coarse.process(m_mask_ds);
    if (m_region_ds.area() == 0)
    {
        dbg_msg("can't locate object");
        return false;
    }

    m_region_orig = upscale_region(m_region_ds, m_scale);
    Mat cropped = bggr(m_region_orig);

    // demosaicing(cropped, m_cropped_bgr, COLOR_BayerRG2BGR);
    cvtColor(cropped, m_cropped_bgr, COLOR_BayerRG2BGR);
    m_color_filter.process_bgr(m_cropped_bgr, m_mask_cr1);
    BinMedian3x3(m_mask_cr1, m_mask_cr);

    //
    // clarify
    //
    Point2i offset = m_region_orig.tl();

    // edge points 
    extract_edge_points(m_mask_cr, m_ball_radius_px, m_edge_points);
    if (m_edge_points.size() < m_expect_inliers * 3 / 4)
    {
        dbg_msg("too few edge points");
        return false;
    }

    vector<Point2f> src(m_edge_points.size());
    vector<Point2f> npts(m_edge_points.size());

    for (int i = 0; i < m_edge_points.size(); ++i)
        src[i] = m_edge_points[i] + offset;

    // get ray-vectors
    cv::undistortPoints(src, npts, m_A, m_distortion, noArray(), Matx33d::eye());
    vector<Vec3d> rays(npts.size());

    for (int i = 0; i < npts.size(); ++i)
        rays[i] = normalized(Vec3d(npts[i].x, npts[i].y, 1.0));

    // get plane of the rays-cone
    m_plane_rns = fit_plane_ransac(rays, m_plane_inliers, m_ball_angle_size / 4, m_fit_ransac_thresh_angle);
    m_plane_lsq = fit_plane_lsq(rays, m_plane_inliers);

    Vec3d v = m_P_inv * m_plane_lsq;
    pt[0] = v[0] / v[2];
    pt[1] = v[1] / v[2];

    return true;
}

void FindBall::draw_inliers(Mat& plot)
{
    plot.create(m_mask_cr.size(), CV_8UC3);
    plot = Scalar(0);

    Vec3f n = m_A * m_plane_lsq;
    CopyChannel(m_mask_cr * 255, plot, 0);
    vector<Point2i> pts;
    get_filtered(m_edge_points, m_plane_inliers, pts);
    DrawPoints(plot, m_edge_points, Scalar(0, 0, 255));
    DrawPoints(plot, pts, Scalar(0, 255, 0));
}

void FindBall::dump(std::string const& prefix)
{
    m_locate_coarse.dump(prefix);

    // if (m_region_ds.area() == 0)
    //     return;

    // if (m_mask_ds1.total() <= 0)
    //     return;

    imgdump(prefix + "_color_filter_bggr_median.png", 255 * m_mask_ds);
    Mat plot(m_mask_ds1.size(), CV_8UC3);
    plot = Scalar(0);
    CopyChannel(m_mask_ds1 * 128, plot, 0);
    DrawRect(plot, m_region_ds, Scalar(0, 255, 0));
    imgdump(prefix + "_color_filter_bggr.png", plot);

    if (m_cropped_bgr.total() <= 0)
        return;

    imgdump(prefix + "_cropped.png", m_cropped_bgr);
    imgdump(prefix + "_color_filter_cr.png", 255 * m_mask_cr1);
    imgdump(prefix + "_color_filter_cr_median.png", 255 * m_mask_cr);

    if (m_edge_points.size() < m_expect_inliers * 3 / 4)
        return;

    draw_inliers(plot);
    imgdump(prefix + "_clarifying.png", plot);
}
