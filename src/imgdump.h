#pragma once

#include <opencv2/opencv.hpp>
#include <cppmisc/json.h>
#include "image_saver.h"

class ImgDumpManager;
using ImgDumpManagerPtr = std::shared_ptr<ImgDumpManager>;


ImgDumpManagerPtr imgdump_init(Json::Value const& cfg);
void imgdump(std::string const& name, cv::Mat const& img, std::string const& type="auto");

class ImgDumpManager
{
    ImageSaver _saver;
    std::string _path;
    bool _active;

    ImgDumpManager();
    void init(Json::Value const& cfg);
    void doit(std::string const& name, cv::Mat const& img, std::string const& type);

public:

    friend ImgDumpManagerPtr imgdump_init(Json::Value const&);
    friend void imgdump(std::string const&, cv::Mat const&, std::string const&);
};
