#pragma once

template <typename T>
inline T in_diap(T x, T a, T b)
{
    return x > a && x < b;
}

template <typename T>
inline T square(T x)
{
    return x * x;
}

template <typename Arr, typename Fun>
inline int argmin(Arr const& arr, Fun const& fun)
{
    int minidx = 0;
    auto minval = fun(arr[0]);

    for (int i = 1; i < arr.size(); ++i)
    {
        auto val = fun(arr[i]);
        if (val < minval)
        {
            minidx = i;
            minval = val;
        }
    }

    return minidx;
}

template <typename T>
inline T minval(std::vector<T> const& arr)
{
    int idx = argmin(arr, [](T const& elem) { return elem; });
    return arr[idx];
}

template <typename T>
inline T maxval(std::vector<T> const& arr)
{
    int idx = argmin(arr, [](T const& elem) { return -elem; });
    return arr[idx];
}

static const double _PI_4 = std::atan(1.0f);
static const double _PI_2 = 2 * _PI_4;
static const double _PI = 4 * _PI_4;
