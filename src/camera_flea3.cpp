#include <string>
#include <sstream>
#include <iostream>
#include <stdexcept>


#include "camera.h"
#include <cppmisc/traces.h>

inline uint32_t timestamp_to_usec(uint32_t ts)
{
    uint32_t nSecond = (ts >> 25) & 0x7F;   // get rid of cycle_* - keep 7 bits
    uint32_t nCycleCount = (ts >> 12) & 0x1FFF; // get rid of offset
    uint32_t nCycleOffset = (ts >> 0) & 0xFFF;   // get rid of *_count

    return 1000000 * nSecond + (125 * nCycleCount + 125 * nCycleOffset / 3072);
}

inline uint32_t get_image_ts_usec(FlyCapture2::Image* img)
{
    FlyCapture2::ImageMetadata const& meta = img->GetMetadata();
    return timestamp_to_usec(meta.embeddedTimeStamp);
}

inline uint32_t sub_ts_usec(uint32_t us2, uint32_t us1)
{
    // maximum value of timestamp is 128 seconds
    // so, each 2 minutes the timestamp overfilles
    // to avoid this we add 128 seconds if an overfill happened
    return us2 - us1 + 128000000 * bool(us1 > us2);
}

static void cam_print_error(FlyCapture2::Error error)
{
    error.PrintErrorTrace();
}

// static void print_build_info()
// {
//     FlyCapture2::FC2Version  fc2Version;
//
//     FlyCapture2::Utilities::GetLibraryVersion(&fc2Version);
//
//  std::cout << "FlyCapture2 library version: " << 
//      fc2Version.major << "." << fc2Version.minor << "." << 
//      fc2Version.type << "." << fc2Version.build << std::endl;
//
//  std::cout << "Application build date: " << __DATE__ << " " << __TIME__ << std::endl;
// }

void camera::printf_info()
{
    FlyCapture2::Error          error;
    FlyCapture2::CameraInfo     cam_info;

    error = m_camera.GetCameraInfo(&cam_info);
    if (error != FlyCapture2::PGRERROR_OK)
    {
        cam_print_error(error);
        throw std::runtime_error("couldn't require camera information");
    }

    std::cout << 
        "\n*** CAMERA INFORMATION ***\n"
        "Serial number - " << cam_info.serialNumber << std::endl <<
        "Camera model - " << cam_info.modelName << std::endl <<
        "Camera vendor - " << cam_info.vendorName << std::endl <<
        "Sensor - " << cam_info.sensorInfo << std::endl <<
        "Resolution - " << cam_info.sensorResolution << std::endl <<
        "Firmware version - " << cam_info.firmwareVersion << std::endl <<
        "Firmware build time - " << cam_info.firmwareBuildTime << std::endl << std::endl;
}

static void find_first_camera(FlyCapture2::PGRGuid& guid)
{
    FlyCapture2::Error      error;
    unsigned int            numCameras;
    FlyCapture2::BusManager busMgr;

    error = busMgr.GetNumOfCameras(&numCameras);
    if (error != FlyCapture2::PGRERROR_OK)
    {
        cam_print_error(error);
        throw std::runtime_error("couldn't initialize bus manager");
    }

    if (numCameras < 1)
    {
        throw std::runtime_error("camera is not connected");
    }

    error = busMgr.GetCameraFromIndex(0, &guid);
    if (error != FlyCapture2::PGRERROR_OK)
    {
        cam_print_error(error);
        throw std::runtime_error("couldn't require camera's GUID");
    }
}

void camera::connect(FlyCapture2::PGRGuid& guid)
{
    FlyCapture2::Error  error;

    error = m_camera.Connect(&guid);
    if (error != FlyCapture2::PGRERROR_OK)
    {
        cam_print_error(error);
        throw std::runtime_error("couldn't connect to camera");
    }
}

void camera::get_max_resolution(int& width, int& height)
{
    FlyCapture2::Error          error;
    FlyCapture2::Format7Info    cam_info;
    bool                        supported;

    error = m_camera.GetFormat7Info(&cam_info, &supported);
    if (error != FlyCapture2::PGRERROR_OK)
    {
        cam_print_error(error);
        throw std::runtime_error("couldn't retrieve camera information");
    }

    width = cam_info.maxWidth;
    height = cam_info.maxHeight;
}

void camera::set_mode7()
{
    FlyCapture2::Error error;

    // set properties
    FlyCapture2::Format7ImageSettings   img_settings;
    FlyCapture2::Format7PacketInfo      packet_info;
    bool                                is_valid = false;

    get_max_resolution(m_width, m_height);

    img_settings.mode = FlyCapture2::MODE_0;
    img_settings.offsetX = 0;
    img_settings.offsetY = 0;
    img_settings.width = m_width;
    img_settings.height = m_height;
    img_settings.pixelFormat = FlyCapture2::PIXEL_FORMAT_RAW8;

    error = m_camera.ValidateFormat7Settings(&img_settings, &is_valid, &packet_info); 
    if (error != FlyCapture2::PGRERROR_OK)
    {
        cam_print_error(error);
        throw std::runtime_error("couldn't apply camera mode");
    }

    if (!is_valid)
    {
        throw std::runtime_error("applying camera parameters are not valid");
    }

    error = m_camera.SetFormat7Configuration(&img_settings, packet_info.recommendedBytesPerPacket);
    if (error != FlyCapture2::PGRERROR_OK)
    {
        cam_print_error(error);
        throw std::runtime_error("couldn't apply 'bytes per packet' parameter");
    }

    // enable embedded timestamp
    FlyCapture2::EmbeddedImageInfo emb_img_info;
    m_camera.GetEmbeddedImageInfo(&emb_img_info);

    if (emb_img_info.timestamp.available)
    {
        emb_img_info.timestamp.onOff = true;
        error = m_camera.SetEmbeddedImageInfo(&emb_img_info);
        if (error != FlyCapture2::PGRERROR_OK)
        {
            cam_print_error(error);
            throw std::runtime_error("can't set embedded image timestamp");
        }
    }
    else
    {
        throw std::runtime_error("embedded timestamp is not supported by camera");
    }

    // white balance
    FlyCapture2::Property white(FlyCapture2::WHITE_BALANCE);
    white.onOff = false;
    white.valueA = 
    white.valueB = 0;

    error = m_camera.SetProperty(&white);
    if (error != FlyCapture2::PGRERROR_OK)
    {
        cam_print_error(error);
        throw std::runtime_error("couldn't apply white balance");
    }

    // exposure value
}

void callback(FlyCapture2::Image* raw, void const* arg)
{
    camera* cam = const_cast<camera*>(
        static_cast<camera const*>(arg)
    );
    cam->callback(raw);
}

void camera::callback(FlyCapture2::Image* raw)
{
    uint32_t t = sysclock.now();
    if (t - m_prev_t > 9000)
        warn_msg("time between 2 frames more than 9ms: ", t - m_prev_t, "us\n");
    m_prev_t = t;

    cv::Mat mat(raw->GetRows(), raw->GetCols(), CV_8U, raw->GetData());

    uint32_t ts = get_image_ts_usec(raw);
    m_handler(mat, ts);
    t = sysclock.now() - t;
    if (t > 3000)
    	warn_msg("image processed in ", t, "usec\n");

    if (ts - m_prev_ts > 9000)
        warn_msg("timestamp differs more than 9ms: ", ts - m_prev_ts, "us\n");
    m_prev_ts = ts;
}

void camera::run()
{
    FlyCapture2::Error error;

    error = m_camera.StartCapture(::callback, this);
    if (error != FlyCapture2::PGRERROR_OK)
    {
        cam_print_error(error);
        throw std::runtime_error("couldn't start capture");
    }

    info_msg("camera launched\n");
}

void camera::stop()
{
    FlyCapture2::Error error;

    error = m_camera.StopCapture();
    if (error == FlyCapture2::PGRERROR_NOT_CONNECTED)
    {
        return;
    }

    if (
        error != FlyCapture2::PGRERROR_OK &&
        error != FlyCapture2::PGRERROR_ISOCH_NOT_STARTED
        )
    {
        cam_print_error(error);
        throw std::runtime_error("couldn't stop capture");
    }

    error = m_camera.Disconnect();
    if (
        error != FlyCapture2::PGRERROR_OK && 
        error != FlyCapture2::PGRERROR_NOT_CONNECTED
        )
    {
        cam_print_error(error);
        throw std::runtime_error("couldn't disconnect camera");
    }

    info_msg("camera stopped\n");
}

camera::camera(image_handler_t const& handler) : 
    m_handler(handler)
{
    FlyCapture2::PGRGuid    guid;

    find_first_camera(guid);
    this->connect(guid);
    this->set_mode7();
    info_msg("camera initialized\n");
}

camera::~camera()
{
    stop();
}

double camera::fps()
{
    FlyCapture2::Error      error;
    FlyCapture2::Property   frame_rate(FlyCapture2::FRAME_RATE);

    error = m_camera.GetProperty(&frame_rate);
    if (error != FlyCapture2::PGRERROR_OK)
    {
        cam_print_error(error);
        throw std::runtime_error("couldn't disconnect camera");
    }

    return frame_rate.absValue;
}
