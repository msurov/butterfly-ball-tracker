#pragma once

#include <string>
#include <functional>
#include <memory>

#include <stdint.h>
#include <cppmisc/throws.h>
#include <cppmisc/traces.h>
#include "json_cvmat.h"

class ICamera;

typedef std::shared_ptr<ICamera> ICameraPtr;
typedef std::function<ICameraPtr(Json::Value const& cfg)> CameraCreator;
typedef std::function<void(uint8_t const* data, int Nx, int Ny, int64_t frame_timestamp)> CameraCallback;


class ICamera
{
public:
    ICamera(ICamera const&) = delete;
    ICamera(ICamera&&) = delete;

    ICamera() {}
    virtual ~ICamera() {}
    virtual void set_handler(CameraCallback const& cb) = 0;
    virtual void run() = 0;
    virtual void stop() = 0;
    virtual std::string description() = 0;
    virtual std::string image_type() = 0;
};


ICameraPtr create_camera(Json::Value const& cfg);
